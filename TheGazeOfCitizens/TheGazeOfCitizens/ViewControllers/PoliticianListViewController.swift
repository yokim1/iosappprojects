//
//  ViewController.swift
//  TheGazeOfCitizens
//
//  Created by 김윤석 on 2021/06/14.
//

//https://open.assembly.go.kr/portal/openapi/nzmimeepazxkubdpn?AGE=21&Type=json&KEY=97aa2b669ea14ffd950f138321724a2a

//https://open.assembly.go.kr/portal/openapi/nwvrqwxyaytdsfvhu?AGE=21&Type=json&KEY=97aa2b669ea14ffd950f138321724a2a

//KEY    STRING(필수)    인증키    기본값 : sample key
//Type    STRING(필수)    호출 문서(xml, json)    기본값 : xml
//pIndex    INTEGER(필수)    페이지 위치    기본값 : 1(sample key는 1 고정)
//pSize    INTEGER(필수)    페이지 당 요청 숫자    기본값 : 100(sample key는 5 고정)

import UIKit
//import McPicker

class PoliticianListViewController: UITableViewController, UISearchBarDelegate,TGCPopUpViewControllerDelegate  {
    func getSelectedAge(_ age: Int) {
        self.age = age
    }
    
    private let searchController = UISearchController(searchResultsController: nil)

    private var politicians: [Politician] = []
    private var filteredData: [Politician] = []
    
    var age: Int?
    
    private var isSearchBarFilled: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(PoliticianListCell.self, forCellReuseIdentifier: PoliticianListCell.identifier)
        
        setupSearchBar()
        
        Network.shared.fetchApps(age: age ?? 0) { politicians, err in
            if let politicians = politicians.last?.row {
                self.politicians = politicians
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "국회 번호", style: .plain, target: self, action: #selector(governmentAgeHandler))
        
        age = 21
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //super.viewWillAppear(animated)
        print("List will appear")
        title = "\(age ?? 0)대 국회"
        
        Network.shared.fetchApps(age: age ?? 0) { politicians, err in
            if let politicians = politicians.last?.row {
                self.politicians = politicians
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("list did appear")
    }

    @objc private func governmentAgeHandler() {
        let vc = TGCPopUpViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func setupSearchBar() {
        navigationItem.searchController = self.searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData.removeAll()
        
        if !searchText.isEmpty {
            isSearchBarFilled = true
        }
        
        for politician in politicians {
//            if politician.HG_NM.starts(with: searchText){
//                filteredData.append(politician)
//            }
            
            if politician.NAME.starts(with: searchText){
                filteredData.append(politician)
            }
            
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filteredData.removeAll()
        isSearchBarFilled = false
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearchBarFilled && filteredData.isEmpty{
            return 0
        }
        
        if !filteredData.isEmpty {
            return filteredData.count
        }
//        print(politicians.count)
        return politicians.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PoliticianListCell.identifier, for: indexPath) as? PoliticianListCell else {return UITableViewCell()}
        
        if isSearchBarFilled && filteredData.isEmpty{
            return UITableViewCell()
        }
        
        if !filteredData.isEmpty {
            cell.configure(politician: filteredData[indexPath.row])
        } else {
            
            cell.configure(politician: politicians[indexPath.row])
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let politicianDetailVC = PoliticianDetailViewController()
        
        if !filteredData.isEmpty {
            politicianDetailVC.data = filteredData[indexPath.row]
        } else {
            politicianDetailVC.data = politicians[indexPath.row]
        }
        
        navigationController?.pushViewController(politicianDetailVC, animated: true)
    }
}
