//
//  PoliticianDetailViewController.swift
//  TheGazeOfCitizens
//
//  Created by 김윤석 on 2021/06/16.
//

import UIKit

class PoliticianDetailViewController: UIViewController {

    var data: Politician?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = data?.NAME
        view.backgroundColor = .red
    }
}
