//
//  TGCPopUpViewController.swift
//  TheGazeOfCitizens
//
//  Created by 김윤석 on 2021/06/16.
//

import UIKit

protocol TGCPopUpViewControllerDelegate {
    func getSelectedAge(_ age: Int)
}

class TGCPopUpViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let ageList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        ageList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
          return ageList[row]
      }
      
      func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedAge = Int(ageList[row])
      }
    
    var selectedAge: Int?
    
    var delegate: PoliticianListViewController?
    
    let containerView : UIView = {
       let view = UIView()
        view.backgroundColor = .systemBackground
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let picker: UIPickerView = {
       let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    let button: UIButton = {
       let button = UIButton()
        button.layer.cornerRadius = 10
        button.backgroundColor = .systemBlue
        button.setTitle("Done", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.75)
        configureContainer()
        configureButton()
        configurePicker()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("TGC disapear")
    }
    
    func configureContainer() {
        view.addSubview(containerView)
        
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            containerView.heightAnchor.constraint(equalToConstant: 280),
            containerView.widthAnchor.constraint(equalToConstant: 250)
        ])
    }
    
    func configureButton() {
        view.addSubview(button)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            button.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            button.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            button.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            button.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    @objc func dismissVC() {
        self.delegate?.getSelectedAge(self.selectedAge ?? 21)
        dismiss(animated: true)
    }
    
    
    func configurePicker() {
        view.addSubview(picker)
        picker.dataSource = self
        picker.delegate = self
        
        NSLayoutConstraint.activate([
            picker.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
            picker.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            picker.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            picker.bottomAnchor.constraint(equalTo: button.topAnchor, constant: -5)
        ])
    }
}
