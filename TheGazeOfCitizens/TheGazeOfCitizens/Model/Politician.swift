//
//  Politician.swift
//  TheGazeOfCitizens
//
//  Created by 김윤석 on 2021/06/15.
//

import Foundation

struct nwvrqwxyaytdsfvhu: Decodable{
    let nprlapfmaufmqytet: [List]
}

struct List: Decodable {
    //let head: [head]
    let row: [Politician]?
}

struct head: Decodable {
    
}
//"head":[
//        {"list_total_count":235},
//        {"RESULT":
//            {"CODE":"INFO-000","MESSAGE":"정상 처리되었습니다."}
//        }
//        ]

struct Politician: Decodable {
//    let name: String
//    let party: String
//    
//    let HG_NM: String
//    let ENG_NM: String?
//    let BTH_DATE: String
//    let POLY_NM: String
    
    let DAESU: String
    let DAE: String
    let DAE_NM: String
    let NAME: String
    let BIRTH: String?
}


//1    DAESU    대수
//2    DAE    대별 및 소속정당(단체)
//3    DAE_NM    대별
//4    NAME    이름
//5    NAME_HAN    이름(한자)
//6    JA    자
//7    HO    호
//8    BIRTH    생년월일
//9    BON    본관
//10    POSI    출생지
//11    HAK    학력 및 경력
//12    HOBBY    종교 및 취미
//13    BOOK    저서
//14    SANG    상훈
//15    DEAD    기타정보(사망일)
//16    URL    회원정보 확인 헌정회 홈페이지 URL

//
//"nwvrqwxyaytdsfvhu": [
//    {
//    "head": [ { "list_total_count": 300 }, { "RESULT": { "CODE": "INFO-000", "MESSAGE": "정상 처리되었습니다." }} ]
//    },
//
//  {
//    "row": [
//      {
//        "HG_NM": "강기윤",
//        "HJ_NM": "姜起潤",
//        "ENG_NM": "KANG GIYUN",
//        "BTH_GBN_NM": "음",
//        "BTH_DATE": "1960-06-04",
//        "JOB_RES_NM": "간사",
//        "POLY_NM": "국민의힘",
//        "ORIG_NM": "경남 창원시성산구",
//        "ELECT_GBN_NM": "지역구",
//        "CMIT_NM": "보건복지위원회",
//        "CMITS": "보건복지위원회",
//        "REELE_GBN_NM": "재선",
//        "UNITS": "제19대, 제21대",
//        "SEX_GBN_NM": "남",
//        "TEL_NO": "02-784-1751",
//        "E_MAIL": "ggotop@naver.com",
//        "HOMEPAGE": "http://blog.naver.com/ggotop",
//        "STAFF": "김홍광, 한영애",
//        "SECRETARY": "박응서, 최광림",
//        "SECRETARY2": "김영록, 안효상, 이유진, 홍지형, 김지훈",
//        "MONA_CD": "14M56632",
//        "MEM_TITLE": "[학력]\r\n마산공고(26회)\r\n창원대학교 행정학과\r\n중앙대학교 행정대학원 지방의회과 석사\r\n창원대학교 대학원 행정학 박사\r\n\r\n[경력]\r\n현) 국회 보건복지위원회 국민의힘 간사\r\n현) 국민의힘 소상공인살리기 특별위원회 부위원장\r\n현) 국민의힘 코로나19 대책 특별위원회 위원\r\n\r\n미래통합당 경남도당 민생특위 위원장\r\n제19대 국회의원 (새누리당/경남 창원시 성산구)\r\n새누리당 원내부대표",
//        "ASSEM_ADDR": "의원회관 937호"
//      }
//    ]
//  }
//]


//Optional(TheGazeOfCitizens.List(row: Optional([TheGazeOfCitizens.Politician(HG_NM: "강기윤", ENG_NM: Optional("KANG GIYUN"), BTH_DATE: "1960-06-04", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "강대식", ENG_NM: Optional("KANG DAESIK"), BTH_DATE: "1959-11-02", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "강득구", ENG_NM: Optional("KANG DEUKGU"), BTH_DATE: "1963-05-27", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "강민국", ENG_NM: Optional("KANG MINKUK"), BTH_DATE: "1971-03-03", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "강민정", ENG_NM: Optional("KANG MINJUNG"), BTH_DATE: "1961-04-26", POLY_NM: "열린민주당"), TheGazeOfCitizens.Politician(HG_NM: "강병원", ENG_NM: Optional("KANG BYUNGWON"), BTH_DATE: "1971-07-09", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "강선우", ENG_NM: Optional("KANG SUNWOO"), BTH_DATE: "1978-06-02", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "강은미", ENG_NM: Optional("KANG EUNMI"), BTH_DATE: "1970-09-06", POLY_NM: "정의당"), TheGazeOfCitizens.Politician(HG_NM: "강준현", ENG_NM: Optional("KANG JUNHYEON"), BTH_DATE: "1964-08-19", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "강훈식", ENG_NM: Optional("KANG HOONSIK"), BTH_DATE: "1973-10-24", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "고민정", ENG_NM: Optional("KO MINJUNG"), BTH_DATE: "1979-08-23", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "고영인", ENG_NM: Optional("KO YOUNGIN"), BTH_DATE: "1963-07-07", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "고용진", ENG_NM: Optional("KOH YONGJIN"), BTH_DATE: "1964-08-06", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "곽상도", ENG_NM: Optional("KWAK SANGDO"), BTH_DATE: "1959-12-23", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "구자근", ENG_NM: Optional("KU JAKEUN"), BTH_DATE: "1967-10-09", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "권명호", ENG_NM: Optional("KWON MYUNGHO"), BTH_DATE: "1961-01-10", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "권성동", ENG_NM: Optional("KWEON SEONGDONG"), BTH_DATE: "1960-04-29", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "권영세", ENG_NM: Optional("KWON YOUNGSE"), BTH_DATE: "1959-02-24", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "권은희", ENG_NM: Optional("KWON EUNHEE"), BTH_DATE: "1974-02-15", POLY_NM: "국민의당"), TheGazeOfCitizens.Politician(HG_NM: "권인숙", ENG_NM: Optional("KWON INSOOK"), BTH_DATE: "1964-08-28", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "권칠승", ENG_NM: Optional("KWON CHILSEUNG"), BTH_DATE: "1965-11-18", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "기동민", ENG_NM: Optional("KI DONGMIN"), BTH_DATE: "1966-02-23", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김경만", ENG_NM: Optional("KIM KYUNGMAN"), BTH_DATE: "1962-09-23", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김경협", ENG_NM: Optional("KIM KYUNGHYUP"), BTH_DATE: "1962-11-07", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김교흥", ENG_NM: Optional("KIM KYOHEUNG"), BTH_DATE: "1960-08-30", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김기현", ENG_NM: Optional("KIM GIHYEON"), BTH_DATE: "1959-02-21", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "김남국", ENG_NM: Optional("KIM NAMKUK"), BTH_DATE: "1982-10-22", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김도읍", ENG_NM: Optional("KIM DOEUP"), BTH_DATE: "1964-07-06", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "김두관", ENG_NM: Optional("KIM DUKWAN"), BTH_DATE: "1959-04-10", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김미애", ENG_NM: Optional("KIM MIAE"), BTH_DATE: "1969-10-06", POLY_NM: "국민의힘"), TheGazeOfCitizens.Politician(HG_NM: "김민기", ENG_NM: Optional("KIM MINKI"), BTH_DATE: "1966-04-28", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김민석", ENG_NM: Optional("KIM MINSEOK"), BTH_DATE: "1964-05-29", POLY_NM: "더불어민주당"), TheGazeOfCitizens.Politician(HG_NM: "김민철", ENG_NM: Optional("KIM MINCHUL"), BTH_DATE: "1967-11-18", POLY_NM: "더불어민주당"),
