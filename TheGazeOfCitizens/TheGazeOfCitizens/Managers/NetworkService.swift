//
//  NetworkService.swift
//  TheGazeOfCitizens
//
//  Created by 김윤석 on 2021/06/15.
//

import Foundation

class Network {
    static let shared = Network()
    
    func fetchApps( age: Int, completion: @escaping ([List], Error?)->()){
        let urlString = "https://open.assembly.go.kr/portal/openapi/nprlapfmaufmqytet?&pSize=300&Type=json&KEY=97aa2b669ea14ffd950f138321724a2a&DAESU=\(age)"
        print(urlString)
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                return
            }
            
            guard let data = data else {return}
            do {
                let searchResult = try JSONDecoder().decode(nwvrqwxyaytdsfvhu.self, from: data)
                
                //print(searchResult.nwvrqwxyaytdsfvhu)
                completion(searchResult.nprlapfmaufmqytet, nil)
                
            } catch let Jsonerror {
                print(Jsonerror)
                print(Jsonerror.localizedDescription)
                print("Failed To Decode")
            }
            
        }.resume()
    }
}
