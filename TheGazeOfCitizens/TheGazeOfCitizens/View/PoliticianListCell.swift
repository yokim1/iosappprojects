//
//  PoliticianListCell.swift
//  TheGazeOfCitizens
//
//  Created by 김윤석 on 2021/06/15.
//

import UIKit

class PoliticianListCell: UITableViewCell {
    static let identifier = "PoliticianListCell"
    
    let nameLabel: UILabel = {
      let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let partyNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func configure(politician: Politician) {
        
        nameLabel.text = politician.NAME
        partyNameLabel.text = politician.DAE
        print(politician.NAME)
        print(politician.DAE)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addSubview(nameLabel)
        addSubview(partyNameLabel)
        
        NSLayoutConstraint.activate([
        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
        nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            
        partyNameLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
        partyNameLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor)
        ])
    }
}
