//
//  MyWatchListViewController.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/15.
//

import UIKit

class MyWatchListViewController: UIViewController, DetailStockViewControllerDelegate {
    
    
    var delegate: UIViewController?
    
    fileprivate let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .black
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    private var stockChartMap: [String: CandleStick]          = [:]
    private var stockPriceMap: [String: StockPriceForTheDay]  = [:]
    
    //private var viewModels: [MyWatchListCell.ViewModel] = []
    
    //    var data: [SymbolSearchResult] = []
    
    var symbolSearchResult: [SymbolSearchResult] = []
    //var marketChartData: [CandleStick] = []
    //var stockPriceForTheDay: [StockPriceForTheDay] = []
    
    var stockDataForUserList: [StockDataForUserList] = []
    
    init(delegate: UIViewController?) {
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var parent2: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //view.backgroundColor = .red
        // Do any additional setup after loading the view.
        //delegate?.navigationController?.hidesBarsOnSwipe = true
        
        fetchWatchlistData()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("will appear MywatchList")
    }
    
    
    private func fetchWatchlistData() {
        let group = DispatchGroup()
        
        if symbolSearchResult.isEmpty {
            group.enter()
            PersistanceManager.shared.retrieveData {[weak self] result in
                defer{
                    group.leave()
                }
                switch result {
                case .success(let results):
                    self?.symbolSearchResult = results
                    break
                    
                case .failure(let error):
                    print(error)
                    break
                }
            }
        }
        
        symbolSearchResult.forEach { symbolData in
            group.enter()
            NetworkManager.shared.fetchStockChartData(for: symbolData.symbol, numberOfDays: 7) {[weak self] result in
                defer {
                    group.leave()
                }
                
                switch result {
                case .success(let data):
                    
                    //DispatchQueue.main.async {
                    //                    self?.marketChartData.append(data)
                    self?.stockChartMap[symbolData.symbol] = data
                    //    self.tableView.reloadData()
                    //}
                    break
                    
                case .failure(let error):
                    print(error)
                    break
                }
            }
            
            group.enter()
            NetworkManager.shared.fetchStockPriceForTheDay(for: symbolData.symbol){[weak self] result in
                defer {
                    group.leave()
                }
                
                switch result {
                case .success(let data):
                    self?.stockPriceMap[symbolData.symbol] = data
                    break
                    
                case .failure(let error):
                    print(error)
                    break
                }
            }
        }
        
        group.notify(queue: .main) {[weak self] in
            print("update user interface")
            self?.tableView.reloadData()
        }
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        
        tableView.contentInset      = .init(top: 0, left: 0, bottom: view.frame.height/1.5, right: 0)
        tableView.dataSource        = self
        tableView.delegate          = self
        tableView.rowHeight         = MyWatchListCell.prefferedHeight
        tableView.register(MyWatchListCell.self, forCellReuseIdentifier: MyWatchListCell.identifier)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func addToMyWatchListButtonDidTap() {
        fetchWatchlistData()
        //tableView.reloadData()
    }
    
}

extension MyWatchListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symbolSearchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyWatchListCell.identifier, for: indexPath) as? MyWatchListCell else {return UITableViewCell()}
        
        if symbolSearchResult.isEmpty != true {
            cell.configure(with: symbolSearchResult[indexPath.row])
        }
        
        //        if marketChartData.isEmpty == false {
        //            cell.configureToMakeChart(with: .init(data: marketChartData[indexPath.row].c ?? []))
        //            cell.configureToMakeCurrentPriceChange(with: stockPriceForTheDay[indexPath.row])
        //        }
        
        if stockChartMap.isEmpty == false {
            cell.configureToMakeChart(with: .init(data: stockChartMap[symbolSearchResult[indexPath.row].symbol]?.c ?? []))
            cell.configureToMakeCurrentPriceChange(with: stockPriceMap[symbolSearchResult[indexPath.row].symbol])
        }
        
        return cell
    }
}

extension MyWatchListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = DetailStockViewController(data: symbolSearchResult[indexPath.row])
        //vc.delegate = self
        //        print(delegate)
        //
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        //delegate?.
        nav.modalPresentationStyle = .overFullScreen
        
        navigationController?.pushViewController(nav, animated: true)
        //
        present(nav, animated: true, completion: nil)
    }
}

extension MyWatchListViewController: SearchResultsViewControllerDelegate {
    func searchResultsViewControllerDidSelect(searchResult: SymbolSearchResult) {
        let vc = DetailStockViewController(data: searchResult)
        //vc.delegate = self
        //        delegate?.navigationController?.pushViewController(vc, animated: true)
        present(vc, animated: true, completion: nil)
    }
    
}
