//
//  SearchViewController.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/02.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate {
    
    private var searchTimer: Timer?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ///This prevents strange space between navigationconrtoller and result showing tableview
        extendedLayoutIncludesOpaqueBars = true
        
        view.backgroundColor = .black
        title = "Search"
        
        navigationBar()
        setUpSearchBar()
        configureBackgroundImage()
    }
    
    private func navigationBar(){
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = .white
    }
    
    private func setUpSearchBar() {
        let resultVC = SearchResultsViewController()
        resultVC.delegate = self
        
        let searchController = UISearchController(searchResultsController: resultVC)
        searchController.searchResultsUpdater = self
        
        navigationItem.searchController = searchController
        searchController.searchBar.tintColor = .white
        searchController.searchBar.searchTextField.textColor = .white
        
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    private func configureBackgroundImage() {
        
        let image = UIImageView(image: UIImage(systemName: "magnifyingglass"))
        image.tintColor = .lightGray
        image.alpha = 0.2
        image.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(image)
        
        NSLayoutConstraint.activate([
            image.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            image.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            image.widthAnchor.constraint(equalToConstant: view.frame.width / 3),
            image.heightAnchor.constraint(equalToConstant: view.frame.width / 3)
        ])
    }
}

extension SearchViewController: UISearchResultsUpdating {
    /// Update search on key tap
    /// - Parameter searchController: Ref of the search controlelr
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text,
              let resultsVC = searchController.searchResultsController as? SearchResultsViewController,
              !query.trimmingCharacters(in: .whitespaces).isEmpty else {
            return
        }

        // Reset timer
        searchTimer?.invalidate()

        // Kick off new timer
        // Optimize to reduce number of searches for when user stops typing
        searchTimer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { _ in
            NetworkManager.shared.search(symbol: query) { result in
                switch result {
                case .success(let response):
                    DispatchQueue.main.async {
                        resultsVC.update(with: response.result)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        resultsVC.update(with: [])
                    }
                    print(error)
                }
            }
        })
    }
}

extension SearchViewController: SearchResultsViewControllerDelegate {
    func searchResultsViewControllerDidSelect(searchResult: SymbolSearchResult) {
        let vc = DetailStockViewController(data: searchResult)
        //navigationController?.pushViewController(vc, animated: true)
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true, completion: nil)
    }
    
}
