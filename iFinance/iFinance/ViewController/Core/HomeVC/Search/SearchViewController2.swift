//
//  SearchViewController2.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/18.
//

import UIKit

class SearchViewController2: UITableViewController {
    
    var delegate: SearchResultsViewControllerDelegate?
    
    enum Section {
        case first
    }
    
    var data: [SymbolSearchResult] = []
    
    var dataSource: UITableViewDiffableDataSource<Section, SymbolSearchResult>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = .systemBackground
        
        tableView.rowHeight         = SearchResultTableViewCell.prefferedHeight
        tableView.register(SearchResultTableViewCell.self, forCellReuseIdentifier: SearchResultTableViewCell.identifier)
        
        extendedLayoutIncludesOpaqueBars = true
        
        navigationBar()
        setUpSearchBar()
        configureDataSource()
    }
    
    private func navigationBar(){
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = .white
        
    }
    
    private func setUpSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        
        navigationItem.searchController = searchController
        searchController.searchBar.tintColor = .white
        searchController.searchBar.searchTextField.textColor = .white
        searchController.obscuresBackgroundDuringPresentation = false
        
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    private func configureDataSource() {
        dataSource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { tableView, indexPath, model -> UITableViewCell in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.identifier, for: indexPath) as! SearchResultTableViewCell
            cell.configure(with: model)
            return cell
        })
        
        dataSource.defaultRowAnimation = .middle
    }
    
    private func updateDataSource(){
        var snapShot = NSDiffableDataSourceSnapshot<Section,SymbolSearchResult>()
        snapShot.appendSections([.first])
        snapShot.appendItems(self.data)
        
        dataSource.apply(snapShot, animatingDifferences: true, completion: nil)
    }
}

extension SearchViewController2: UISearchResultsUpdating {
    /// Update search on key tap
    /// - Parameter searchController: Ref of the search controlelr
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text,
              !query.trimmingCharacters(in: .whitespaces).isEmpty else {
            return
        }
        
        NetworkManager.shared.search(symbol: query) { result in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self.data = response.result
                    self.updateDataSource()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    
                }
                print(error)
            }
        }
    }
}

extension SearchViewController2 {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailStockViewController(data: data[indexPath.row])
        //navigationController?.pushViewController(vc, animated: true)
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true, completion: nil)
        
    }
}
