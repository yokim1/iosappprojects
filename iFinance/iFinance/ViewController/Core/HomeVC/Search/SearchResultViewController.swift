//
//  SearchResultViewController.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/10.
//

import UIKit

/// Delegate for search resutls
protocol SearchResultsViewControllerDelegate: AnyObject {
    /// Notify delegate of selection
    /// - Parameter searchResult: Result that was picked
    func searchResultsViewControllerDidSelect(searchResult: SymbolSearchResult)
}

/// VC to show search results
//final class SearchResultsViewController: UITableViewController {
//
//    var data: [SymbolSearchResult] = []
//
//    var delegate: SearchResultsViewControllerDelegate?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableView.backgroundColor = .systemBackground
//
//        tableView.rowHeight         = SearchResultTableViewCell.prefferedHeight
//        tableView.register(SearchResultTableViewCell.self, forCellReuseIdentifier: SearchResultTableViewCell.identifier)
//    }
//
//    public func update(with result: [SymbolSearchResult]) {
//        data = result
//        tableView.reloadData()
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        data.count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.identifier, for: indexPath) as? SearchResultTableViewCell else {
//            return UITableViewCell() }
//        cell.configure(with: data[indexPath.row])
//
//        return cell
//    }
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        delegate?.searchResultsViewControllerDidSelect(searchResult: data[indexPath.row])
//    }
//}

final class SearchResultsViewController: UITableViewController {
    
    var data: [SymbolSearchResult] = []
    
    var delegate: SearchResultsViewControllerDelegate?
    
    enum Section {
        case first
    }
    
    struct Data: Hashable {
        let title: String
    }
    
    var dataSource: UITableViewDiffableDataSource<Section, Data>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .systemBackground
        
        tableView.rowHeight         = SearchResultTableViewCell.prefferedHeight
        tableView.register(SearchResultTableViewCell.self, forCellReuseIdentifier: SearchResultTableViewCell.identifier)
        
        dataSource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { tableView, indexPath, model -> UITableViewCell in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.identifier, for: indexPath) as! SearchResultTableViewCell
            cell.textLabel?.text = model.title
            return cell
        })
    }
    
    public func update(with result: [SymbolSearchResult]) {
        data = result
        tableView.reloadData()
    }
    
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        data.count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.identifier, for: indexPath) as? SearchResultTableViewCell else {
//            return UITableViewCell() }
//        cell.configure(with: data[indexPath.row])
//
//        return cell
//    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.searchResultsViewControllerDidSelect(searchResult: data[indexPath.row])
    }
}
