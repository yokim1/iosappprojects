//
//  CommunityCommentViewController.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/19.
//

import UIKit

class CommunityCommentViewController: UIViewController {

    let tableView: UITableView = {
       let tv = UITableView()
        tv.register(CommunityCommentTableViewCell.self, forCellReuseIdentifier: CommunityCommentTableViewCell.identifier)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    
    private func configureTableView() {
        view.addSubview(tableView)
//        tableView.dataSource    = self
//        tableView.delegate      = self
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
