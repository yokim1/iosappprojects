////
////  DetailStockViewController.swift
////  iFinance
////
////  Created by 김윤석 on 2021/08/03.
////
//
//import FloatingPanel
//import UIKit
//
//class DetailStockViewController: UIViewController, FloatingPanelControllerDelegate {
//
//    private let tableView: UITableView = {
//        let tv = UITableView()
//        tv.allowsSelection = false
//        tv.register(StockDetailChartCell.self, forCellReuseIdentifier: StockDetailChartCell.identifier)
//        return tv
//    }()
//
//    /// Stock symbol
//    private let symbol: String
//
//    /// Company name
//    private let companyName: String
//
//    /// Collection of data
//    private var candleStickData: CandleStick?
//
//    /// Company metrics
//    private var metrics: Metrics?
//
//    //var closeData: [Double] = []
//
//    var closeData: [[Double]] = []
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        navigationItem.hidesSearchBarWhenScrolling = true
//
//        configureNavRightButton()
//        configureTableView()
//        configureFloatingPanel()
//        fetchChartData()
//    }
//
//    //    init(symbol: String, companyName: String, candleStickData: [CandleStick] = []) {
//    //        self.symbol = symbol
//    //        self.companyName = companyName
//    //        self.candleStickData = candleStickData
//    //        super.init(nibName: nil, bundle: nil)
//    //    }
//
//    init(data: SymbolSearchResult) {
//        symbol          = data.displaySymbol
//        companyName     = data.description
//        candleStickData = nil
//        super.init(nibName: nil, bundle: nil)
//
//    }
//
//    private func configureNavRightButton() {
//        let addButton = UIBarButtonItem(image: UIImage(systemName: "plus.app"), style: .done, target: self, action: #selector(addButtonDidTapped))
//        navigationItem.rightBarButtonItems = [addButton]
//    }
//
//    @objc private func addButtonDidTapped(){
//        print("Added")
//    }
//
//    private func fetchChartData() {
//
//        NetworkManager.shared.fetchStockChartData(for: symbol) { result in
//            switch result {
//            case .success(let datas):
//
////                self.closeData = datas.c ?? []
//
//                DispatchQueue.main.async {
//                    self.closeData.append(datas.c ?? [])
//                    self.candleStickData = datas
//                    self.tableView.reloadData()
//                }
//                break
//
//            case .failure(let error):
//                print(error)
//                break
//            }
//        }
//
//        let numberOfDays: [TimeInterval] = [5, 15, 30]
//
//        numberOfDays.forEach { numberOfDay in
//            NetworkManager.shared.fetchStockChartData(for: symbol, numberOfDays: numberOfDay) { result in
//                switch result{
//                case .success(let data):
//
//                    DispatchQueue.main.async {
//                        self.closeData.append(data.c ?? [])
//                        self.tableView.reloadData()
//                    }
//                    break
//
//                case .failure(let error):
//                    print(error)
//                    break
//                }
//            }
//        }
//
//    }
//
//    private func configureTableView() {
//        view.addSubview(tableView)
//        tableView.layer.frame       = view.bounds
//        tableView.dataSource        = self
//        tableView.delegate          = self
//    }
//
//    private func configureFloatingPanel() {
//        let vc = StockNewsAndCommentsViewController(symbol: "")
//        let panel = FloatingPanelController(delegate: self)
//        panel.surfaceView.backgroundColor = .secondarySystemBackground
//        panel.set(contentViewController: vc)
//        panel.addPanel(toParent: self)
//        panel.track(scrollView: vc.tableView)
//        panel.delegate = self
//    }
//
//    private func renderChart() {
//
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//}
//
//extension DetailStockViewController: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: StockDetailChartCell.identifier, for: indexPath) as? StockDetailChartCell
//        //let data = candleStickData
//        else { return UITableViewCell() }
////        cell.configureToMakeChart(with: .init(data: candleStickData?.c ?? []))
//
//        if closeData.isEmpty {
//            return UITableViewCell()
//        }
//
//        print(closeData.count)
//        cell.configureToMakeChart(with: .init(data: closeData[0]))
////        cell.configureToMakeChart(with: closeData[0])
//        cell.configure(with: closeData)
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return view.frame.height
//    }
//}
//
//extension DetailStockViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: false)
//
//        print("\(indexPath.row)")
//    }
//}
//
///// Metrics response from API
//struct FinancialMetricsResponse: Codable {
//    let metric: Metrics
//}
//
///// Financial metrics
//struct Metrics: Codable {
//    let TenDayAverageTradingVolume: Float
//    let AnnualWeekHigh: Double
//    let AnnualWeekLow: Double
//    let AnnualWeekLowDate: String
//    let AnnualWeekPriceReturnDaily: Float
//    let beta: Float
//
//    enum CodingKeys: String, CodingKey {
//        case TenDayAverageTradingVolume = "10DayAverageTradingVolume"
//        case AnnualWeekHigh = "52WeekHigh"
//        case AnnualWeekLow = "52WeekLow"
//        case AnnualWeekLowDate = "52WeekLowDate"
//        case AnnualWeekPriceReturnDaily = "52WeekPriceReturnDaily"
//        case beta = "beta"
//    }
//}

//
//  DetailStockViewController.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/03.
//

import FloatingPanel
import UIKit

protocol DetailStockViewControllerDelegate: AnyObject {
    func addToMyWatchListButtonDidTap()
}

class DetailStockViewController: UIViewController, FloatingPanelControllerDelegate {
    
    var delegate: DetailStockViewControllerDelegate?
    
    private let tableView: UITableView = {
        let tv = UITableView()
        tv.allowsSelection = false
        tv.register(StockDetailChartCell.self, forCellReuseIdentifier: StockDetailChartCell.identifier)
        return tv
    }()
    
    /// Collection of data
    private var candleStickData: CandleStick?
    //private var candleStickData: [CandleStick] = []
    
    //var closeData: [Double] = []
    
    var stockSymbolSearchResult: SymbolSearchResult?
    
    var closeData: [[Double]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = stockSymbolSearchResult?.symbol
        navigationItem.hidesSearchBarWhenScrolling = true
        navigationController?.navigationBar.tintColor = .white
        
        configureNavigationBarItemColor()
        configureNavRightButton()
        configureTableView()
        configureFloatingPanel()
        fetchChartData()
    }
    
    init(data: SymbolSearchResult) {
//        symbol          = data.displaySymbol
//        companyName     = data.description
        self.stockSymbolSearchResult = data
        candleStickData = nil
        //candleStickData = []
        super.init(nibName: nil, bundle: nil)
    }
    
    private func configureNavigationBarItemColor() {
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    lazy var addButton: UIBarButtonItem = {
       let button = UIBarButtonItem(image: UIImage(systemName: "plus.app"), style: .done, target: self, action: #selector(addButtonDidTapped))
        return button
    }()
    
    lazy var closeButton: UIBarButtonItem = {
       let button = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(closeButtonDidTap))
        return button
    }()
    
    private func configureNavRightButton() {
        navigationItem.rightBarButtonItems  = [addButton]
        navigationItem.leftBarButtonItems   = [closeButton]
    }
    
    @objc private func addButtonDidTapped() {
        guard let data = stockSymbolSearchResult else { return }
        PersistanceManager.shared.update(favorite: data, actionType: .add) { _ in }
        NotificationCenter.default.post(name: .didAddToWatchList, object: nil)
        delegate?.addToMyWatchListButtonDidTap()
    }
    
    @objc private func closeButtonDidTap() {
        dismiss(animated: true, completion: nil)
    }
    
    private func fetchChartData() {
        
//        NetworkManager.shared.fetchStockChartData(for: stockSymbolSearchResult?.symbol ?? "", numberOfDays: 1) { result in
//            switch result {
//            case .success(let datas):
//
//                DispatchQueue.main.async {
//                    self.closeData.append(datas.c ?? [])
//                    self.candleStickData = datas
//                    print(self.candleStickData)
//                    self.tableView.reloadData()
//                }
//                break
//
//            case .failure(let error):
//                print(error)
//                break
//            }
//        }
        
        let numberOfDays: [TimeInterval] = [1, 5, 15, 30, 180, 350]
        
        numberOfDays.forEach { numberOfDay in
            print(numberOfDay)
            
            NetworkManager.shared.fetchStockChartData(for: stockSymbolSearchResult?.symbol ?? "", numberOfDays: numberOfDay) { result in
                
                switch result {
                case .success(let data):
                    
                    DispatchQueue.main.async {
                        
                        self.closeData.append(data.c ?? [])
                        self.candleStickData = data
                        self.tableView.reloadData()
                    }
                    break
                    
                case .failure(let error):
                    print(error)
                    break
                }
            }
        }
        
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        
        tableView.contentInset      = .init(top: 0, left: 0, bottom: 0, right: 0)
        tableView.layer.frame       = view.bounds
        tableView.dataSource        = self
        tableView.delegate          = self
    }
    
    private func configureFloatingPanel() {
        let vc = StockNewsAndCommentsViewController(symbol: stockSymbolSearchResult?.symbol ?? "")
        let panel = FloatingPanelController(delegate: self)
        panel.surfaceView.backgroundColor = .secondarySystemBackground
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
        panel.delegate = self
        
        let appearance = SurfaceAppearance()
        appearance.backgroundColor = .black
        appearance.cornerRadius = 10
        panel.surfaceView.appearance = appearance
    }
    
    private func renderChart() {
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension DetailStockViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StockDetailChartCell.identifier, for: indexPath) as? StockDetailChartCell
        //let data = candleStickData
        else { return UITableViewCell() }
//        cell.configureToMakeChart(with: .init(data: candleStickData?.c ?? []))
        
        
//        if closeData.isEmpty {
//            return UITableViewCell()
//        }
        
        //guard let data = candleStickData.first?.c else { return UITableViewCell()}
        
//        cell.configureToMakeChart(with: closeData[0])
        cell.configureToMakeChart(with: .init(data: candleStickData?.c ?? []))
        //cell.configureToMakeChart(with: .init(data: data))
        cell.configure(with: closeData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height
    }
}

extension DetailStockViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        print("\(indexPath.row)")
    }
}

extension DetailStockViewController {
    func floatingPanelDidChangeState(_ fpc: FloatingPanelController) {
        if fpc.state == .full {
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white.withAlphaComponent(0.5)]
            closeButton.tintColor = .white.withAlphaComponent(0.5)
            addButton.tintColor = .white.withAlphaComponent(0.5)
        } else {
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white.withAlphaComponent(1)]
            closeButton.tintColor = .white.withAlphaComponent(1)
            addButton.tintColor = .white.withAlphaComponent(1)
        }
    }
}
