//
//  HomeViewController.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/02.
//

import FloatingPanel
import UIKit

class BaseViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    lazy var searchButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .done, target: self, action: #selector(searchButtonDidTap))
        button.tintColor = .white
        return button
    }()
    
    lazy var editButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(systemName: "line.horizontal.3"), style: .done, target: self, action: #selector(editButtonDidTap))
        button.tintColor = .white
        return button
    }()
    
    let menuBar: MenuTabBarView = {
        let menuBar = MenuTabBarView()
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        return menuBar
    }()
    
    private var panel: FloatingPanelController?
    
    //    fileprivate let collectionView: UICollectionView = {
    //       let layout = UICollectionViewFlowLayout()
    //        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    //        cv.register(MyWatchListCollectionViewCell.self, forCellWithReuseIdentifier: MyWatchListCollectionViewCell.identifier)
    //        cv.register(CommunityCommentCollectionViewCell.self, forCellWithReuseIdentifier: CommunityCommentCollectionViewCell.identifier)
    //        cv.translatesAutoresizingMaskIntoConstraints = false
    //        return cv
    //    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .black
        //navigationController?.hidesBarsOnSwipe = true
        
        configureTitleView()
        configureRightBarButton()
        
        configureMenuBar()
        configureSwipingViewControllers()
        
        configureFloatingPanel()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // navigationController?.setNavigationBarHidden(false, animated: true)
      
    }
    
    private func configureTitleView() {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.text             = "  iFinance"
        titleLabel.textColor        = .white
        titleLabel.font             = UIFont.boldSystemFont(ofSize: 32)
        navigationItem.titleView    = titleLabel
    }
    
    private func configureRightBarButton() {
        navigationItem.rightBarButtonItems      = [editButton, searchButton]
    }
    
    @objc private func searchButtonDidTap() {
        let vc = SearchViewController2()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func editButtonDidTap() {
        
    }
    
    private func configureMenuBar() {
        
        menuBar.baseViewController = self
        view.addSubview(menuBar)
        
        NSLayoutConstraint.activate([
            menuBar.topAnchor.constraint(equalTo: view.topAnchor),
            menuBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            menuBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            menuBar.heightAnchor.constraint(equalToConstant: Constants.menuTabBarHeight),
        ])
    }
    
    private func configureFloatingPanel() {
        let vc = NewsViewController()
        let panel = FloatingPanelController(delegate: self)
        panel.surfaceView.backgroundColor = .secondarySystemBackground
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
        panel.delegate = self
        
        let appearance = SurfaceAppearance()
        appearance.backgroundColor = .black
        appearance.cornerRadius = 10
        panel.surfaceView.appearance = appearance
    }
    
    private func configureSwipingViewControllers() {
        
        collectionView.register(MyWatchListViewControllerCollectionViewCell.self, forCellWithReuseIdentifier: MyWatchListViewControllerCollectionViewCell.identifier)
//        collectionView.register(MyWatchListCollectionViewCell.self, forCellWithReuseIdentifier: MyWatchListCollectionViewCell.identifier)
        collectionView.register(CommunityCommentViewControllerCollectionViewCell.self, forCellWithReuseIdentifier: CommunityCommentViewControllerCollectionViewCell.identifier)
        collectionView.isPagingEnabled = true
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / CGFloat(Constants.numberOfItems)
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
    }
    
    func scrollToMenuIndex(menuIndex: Int) {
        collectionView.isPagingEnabled = false
        let indexPath = IndexPath(row: menuIndex, section: 0)
        collectionView.scrollToItem(at: indexPath, at: [], animated: true)
        collectionView.isPagingEnabled = true
    }
}

extension BaseViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        2
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyWatchListViewControllerCollectionViewCell.identifier, for: indexPath) as! MyWatchListViewControllerCollectionViewCell
            cell.parent = self
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CommunityCommentViewControllerCollectionViewCell.identifier, for: indexPath)
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        .init(top: Constants.menuTabBarHeight * 2 + 34, left: 0, bottom: 0, right: 0)
    }
}

extension BaseViewController: FloatingPanelControllerDelegate {
    /// Gets floating panel state change
    /// - Parameter fpc: Ref of controller
    func floatingPanelDidChangeState(_ fpc: FloatingPanelController) {
        
        if fpc.state == .full {
            navigationItem.titleView?.alpha = 0.2
            searchButton.tintColor = .white.withAlphaComponent(0.5)
            editButton.tintColor = .white.withAlphaComponent(0.5)
            
            searchButton.isEnabled = false
            editButton.isEnabled = false
        } else {
            navigationItem.titleView?.alpha = 1
            searchButton.tintColor = .white.withAlphaComponent(1)
            editButton.tintColor = .white.withAlphaComponent(1)
            
            searchButton.isEnabled = true
            editButton.isEnabled = true
        }
    }
}
