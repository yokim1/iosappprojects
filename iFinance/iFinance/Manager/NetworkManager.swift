//
//  NetworkManager.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/03.
//

import UIKit

enum DataType {
    case news
    case stock
}

enum APIError: Error {
    case noDataReturned
    case invalidUrl
}

class NetworkManager {
    static let shared = NetworkManager()
    
    private let newsUrl = "https://finnhub.io/api/v1/news?category=general"
    private let quoteUrl = "https://finnhub.io/api/v1/quote?symbol="
    private let specificNewsUrl = "https://finnhub.io/api/v1/company-news?symbol="
    private let stockCandleUrl = "https://finnhub.io/api/v1/stock/candle?"
    private let searchStockUrl = "https://finnhub.io/api/v1/search?q="
    private let metricUrl = "https://finnhub.io/api/v1/stock/metric?metric=all&symbol="
    private let apiKey = "&token=c3ecka2ad3ief4elg710"
    
    func fetchGeneralNews(completionHandler: @escaping (Result<[NewsStory], APIError>) -> Void){
        let urlString = newsUrl + apiKey
        request(for: urlString, expectingDataType: [NewsStory].self, completionHandler: completionHandler)
    }
    
    func fetchSpecificCompanyNews(for symbol: String, completionHandler: @escaping (Result<[NewsStory], APIError>)-> Void){
        let day: TimeInterval = 3600 * 24
        let today = Date()
        let oneMonthBack = today.addingTimeInterval(-(day * 7))
        
        //https://finnhub.io/api/v1/company-news?symbol=FB&from=2021-07-27&to=2021-08-03&token=c3c6me2ad3iefuuilms0
        
        let from = "from=\(DateFormatter.newsDateFormatter.string(from: oneMonthBack))"
        let to = "to=\(DateFormatter.newsDateFormatter.string(from: today))"
        
        let urlString = specificNewsUrl + symbol + apiKey + "&" + from + "&" + to
        print(urlString)
        
        request(for: urlString, expectingDataType: [NewsStory].self, completionHandler: completionHandler)
    }
    
    func fetchStockPriceForTheDay(for symbol: String, completionHandler: @escaping (Result<StockPriceForTheDay, APIError>) -> Void) {
        //https://finnhub.io/api/v1/quote?symbol=AAPL&token=c3ecka2ad3ief4elg710
        
        let urlString = quoteUrl + symbol + apiKey
        
        request(for: urlString, expectingDataType: StockPriceForTheDay.self, completionHandler: completionHandler)
    }
    
    func fetchStockChartData(for symbol: String, numberOfDays: TimeInterval = 7, completionHandler: @escaping (Result<CandleStick, APIError>) -> Void) {
        //original
        //https://finnhub.io/api/v1/stock/candle?symbol=AAPL&resolution=1&from=1615298999&to=1615302599&token=c3ecka2ad3ief4elg710
        
        //yours
        //https://finnhub.io/api/v1/stock/candle?&symbol=NRGU&resolution=1&from=1628542376&to=1628628776&token=c3ecka2ad3ief4elg710
        
        let today = Date().addingTimeInterval(-(Constants.day))
        let prior = today.addingTimeInterval(-(Constants.day * (numberOfDays)))
        
        let symbolQuery = "&symbol=\(symbol)"
        var resolution = "&resolution=" //1, 5, 15, 30, 60, D, W, M
        let from = "&from=\(Int(prior.timeIntervalSince1970))"
        let to = "&to=\(Int(today.timeIntervalSince1970))"
        
//        switch numberOfDays {
//        case 1:
//            resolution += "1"
//            break
//
//        case 5:
//            resolution += "5"
//            break
//
//        case 15:
//            resolution += "15"
//            break
//
//        case 30:
//            resolution += "30"
//            break
//
//        case 60:
//            resolution += "60"
//            break
//
//        default:
//            return
//        }
        
        resolution += "1"
        
        let urlString = stockCandleUrl + symbolQuery + resolution + from + to + apiKey
        
        print(urlString)
        
        request(for: urlString, expectingDataType: CandleStick.self, completionHandler: completionHandler)
    }
    
    func search(symbol: String, completionHandler: @escaping (Result<SymbolSearchResponse, APIError>) -> Void) {
        let urlString = searchStockUrl + symbol + apiKey
        
        request(for: urlString, expectingDataType: SymbolSearchResponse.self, completionHandler: completionHandler)
    }
    
    
    private func request<T: Codable>(for urlString: String, expectingDataType: T.Type, completionHandler: @escaping (Result<T, APIError>)->Void) {
        guard let url = URL(string: urlString) else {
            completionHandler(.failure(.invalidUrl))
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data,
                  nil == error else {
                completionHandler(.failure(.noDataReturned))
                return
            }
            
            do {
                let result = try JSONDecoder().decode(expectingDataType, from: data)
                completionHandler(.success(result))
            } catch {
                print(error)
                completionHandler(.failure(.noDataReturned))
            }
            
        }.resume()
    }
}
