//
//  PersistanceManager.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/03.
//

import UIKit

enum PersistanceActionType {
    case add, remove
}

class PersistanceManager {
    static let shared = PersistanceManager()
    
    enum Keys {
        static let favorites = "MyList"
    }
    
    func update(favorite: SymbolSearchResult, actionType: PersistanceActionType, completed: @escaping (GFError?) -> Void) {
        retrieveData { results in
            switch results {
            case .success(let favorites):
                var retrievedFavorites = favorites
                switch  actionType {
                case .add:
                    guard !retrievedFavorites.contains(favorite) else {
                        completed(.alreadyExist)
                        return
                    }
                    retrievedFavorites.append(favorite)

                case .remove:
                    retrievedFavorites.removeAll { follower in
                        favorite.symbol == follower.symbol
                    }
                }

                completed(self.save(favorites: retrievedFavorites))

            case.failure(let error):
                completed(error)
            }
        }
    }
    
    func retrieveData(completed: @escaping (Result<[SymbolSearchResult], GFError>) -> Void) {
        guard let favoritesData = UserDefaults.standard.object(forKey: Keys.favorites) as? Data else {
            completed(.success([]))
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let followers = try decoder.decode([SymbolSearchResult].self, from: favoritesData)
            completed(.success(followers))
        } catch {
            completed(.failure(.invalidData))
        }
    }
    
    func save(favorites: [SymbolSearchResult]) -> GFError? {
        do{
            let encoder = JSONEncoder()
            let encodedFavorites = try encoder.encode(favorites)
            UserDefaults.standard.setValue(encodedFavorites, forKey: Keys.favorites)
            return nil
        } catch {
            return .unableToComplete
        }
    }
}

enum GFError: String, Error {
    case invalidUsername = "invalid username"
    case unableToComplete = "unable to complete"
    case invalidResponse = "invalid response"
    case invalidData = "invalid data"
    case alreadyExist = "The data already exist"
}
