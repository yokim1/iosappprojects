//
//  NewsHeaderView.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/03.
//

import UIKit

/// Delegate to notify of header evnets
protocol NewsHeaderViewDelegate: AnyObject {
    /// Notify user tapped header button
    /// - Parameter headerView: Ref of header view
    func newsHeaderViewDidTapAddButton(_ headerView: NewsHeaderView)
}

/// TableView header for news
final class NewsHeaderView: UITableViewHeaderFooterView {
    /// Header identifier
    static let identifier = "NewsHeaderView"

    /// Ideal height of header
    static let preferredHeight: CGFloat = 70

    /// Delegate instance for evnets
    weak var delegate: NewsHeaderViewDelegate?

    // MARK: - Private

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 32)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let button: UIButton = {
        let button = UIButton()
        button.setTitle("+ Watchlist", for: .normal)
        button.backgroundColor = .systemBlue
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        return button
    }()

    // MARK: - Init
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .secondarySystemBackground
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        
        addSubview(titleLabel)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
//        titleLabel.frame = CGRect(x: 14, y: 0, width: contentView.width-28, height: contentView.height)
//
//        button.sizeToFit()
//        button.frame = CGRect(
//            x: contentView.width - button.width - 16,
//            y: (contentView.height - button.height)/2,
//            width: button.width + 8,
//            height: button.height
//        )
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.leadingPadding),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
    }

    /// Handle button tap
    @objc private func didTapButton() {
        // Call delegate
        delegate?.newsHeaderViewDidTapAddButton(self)
    }

    /// Configure view
    /// - Parameter viewModel: View ViewModel
    public func configure(with title: String) {
        titleLabel.text = title
    }
}
