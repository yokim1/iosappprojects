//
//  StockDetailChart.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/12.
//

import Charts
import UIKit

struct ViewModel {
    var data: [Double]
}

class StockDetailChartCell: UITableViewCell {

    static let identifier = "StockDetailChartCell"

    var viewModel: ViewModel = .init(data: [])
    
    var data:[[Double]] = []
    
    private let chartView: LineChartView = {
       let chart = LineChartView()
        chart.pinchZoomEnabled = false
        chart.setScaleEnabled(true)
        chart.xAxis.enabled = false
        chart.drawGridBackgroundEnabled = false
        chart.leftAxis.enabled = false
        //chart.rightAxis.enabled = false
        chart.rightAxis.labelTextColor = .white
        chart.legend.enabled = false
        chart.translatesAutoresizingMaskIntoConstraints = false
        return chart
    }()
    
    private let daysSelector: UISegmentedControl = {
        let items = ["1D", "5D", "15D", "30D", "6M", "1Y"]
        let segmentControl = UISegmentedControl(items: items)
        segmentControl.selectedSegmentIndex = 0
        segmentControl.selectedSegmentTintColor = UIColor(red: 88/255, green: 111/255, blue: 135/255, alpha: 1)
        segmentControl.backgroundColor = .lightText
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentControl
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.isUserInteractionEnabled = false
        backgroundColor = .black
        
        configureChart()
        configureDaysSelector()
    }

    private func configureChart() {
        addSubview(chartView)
        
        NSLayoutConstraint.activate([
            chartView.leadingAnchor.constraint(equalTo: leadingAnchor),
            chartView.topAnchor.constraint(equalTo: topAnchor),
            chartView.trailingAnchor.constraint(equalTo: trailingAnchor),
            chartView.heightAnchor.constraint(equalToConstant: frame.width)
        ])
    }
    
    private func configureDaysSelector() {
        addSubview(daysSelector)
        daysSelector.addTarget(self, action: #selector(daySelectorDidChange(_ :)), for: .valueChanged)
        
        let padding: CGFloat = 10
        
        NSLayoutConstraint.activate([
            daysSelector.leadingAnchor.constraint(equalTo: chartView.leadingAnchor, constant: padding),
            daysSelector.trailingAnchor.constraint(equalTo: chartView.trailingAnchor, constant: -padding),
            daysSelector.topAnchor.constraint(equalTo: chartView.bottomAnchor),
        ])
    }
    
    @objc private func daySelectorDidChange(_ segmentControl: UISegmentedControl) {
        
        if data.isEmpty {
            configureToMakeChart(with: .init(data: []))
            return
        }
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
           configureToMakeChart(with: .init(data: data[0]))
            break
        
        case 1:
            self.configureToMakeChart(with: .init(data: data[1]))
            break
        
        case 2:
            configureToMakeChart(with: .init(data: data[2]))
            break
        
        case 3:
            configureToMakeChart(with: .init(data: data[3]))
            break
            
        case 4:
            configureToMakeChart(with: .init(data: data[4]))
            break
            
        case 5:
            configureToMakeChart(with: .init(data: data[5]))
            break
            
        default:
            return
        }
    }
    
    func configure(with datas: [[Double]]) {
        self.data = datas
    }

    func configureToMakeChart(with viewModel: ViewModel) {
    //func configureToMakeChart(with data: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []

        for (index, value) in viewModel.data.enumerated() {
            dataEntries.append(
                .init(
                    x: Double(index),
                    y: value))
        }
        
        let dataSet = LineChartDataSet(entries: dataEntries, label: "Some Label")
        dataSet.fillColor = .systemBlue
        dataSet.drawFilledEnabled = true
        dataSet.drawIconsEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.drawCirclesEnabled = false

        let data = LineChartData(dataSet: dataSet)
        chartView.data = data
        
        chartView.animate(yAxisDuration: 0.4, easingOption: .linear)
//        chartView.animate(xAxisDuration: 3.0, yAxisDuration: 3.0, easingOption: .easeOutQuad)

//        chartView.data?.notifyDataChanged()
//        chartView.notifyDataSetChanged()
    }
    
//    func configureToMakeChart(with data: [Double]) {
//    //func configureToMakeChart(with data: [Double]) {
//        print(data.count)
//
//        var dataEntries: [ChartDataEntry] = []
//
//        for (index, value) in data.enumerated() {
//            dataEntries.append(
//                .init(
//                    x: Double(index),
//                    y: value))
//
//            print(index)
//            print(value)
//        }
//
//        let dataSet = LineChartDataSet(entries: dataEntries, label: "Some Label")
//        dataSet.fillColor = .systemBlue
//        dataSet.drawFilledEnabled = true
//        dataSet.drawIconsEnabled = false
//        //dataSet.drawValuesEnabled = false
//        dataSet.drawCirclesEnabled = false
//
//        let data = LineChartData(dataSet: dataSet)
//        chartView.data = data
////
//        chartView.data?.notifyDataChanged()
//        chartView.notifyDataSetChanged()
//    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
