//
//  MyWatchListCell.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/03.
//

import Charts
import UIKit

class MyWatchListCell: UITableViewCell {
    
    static let identifier                   = "MyWatchListCell"
    static let prefferedHeight: CGFloat     = 70
    
//    struct ViewModel {
//        let symbol: String
//        let companyName: String
//        let price: String // formatted
//        let changeColor: UIColor // red or green
//        let changePercentage: String // formatted
//        //let chartViewModel: StockChartView.ViewModel
//    }

    private let symbolLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let fullNameLabel: UILabel = {
       let label = UILabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
//    private let previewChart: ChartView = {
//       let chart = ChartView()
//        chart.translatesAutoresizingMaskIntoConstraints = false
//        chart.clipsToBounds = true
//        return chart
//    }()
    
    private let previewChart: LineChartView = {
        let chartView = LineChartView()
        chartView.pinchZoomEnabled = false
        //chartView.setScaleEnabled(true)
        chartView.xAxis.enabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.legend.enabled = false
        chartView.leftAxis.enabled = false
        chartView.rightAxis.enabled = false
        chartView.clipsToBounds = true
        chartView.translatesAutoresizingMaskIntoConstraints = false
        return chartView
    }()
    
    private let currentPriceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = .white
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let priceChangeLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.textAlignment = .right
        label.layer.cornerRadius = 3
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .black
        
        configureTitleLabels()
        configurePriceLabels()
        configureChart()
    }
    
    private func configureTitleLabels() {
        
        symbolLabel.text = "Symbol"
        fullNameLabel.text = "Company Name"
        
        let labelStackView = UIStackView(arrangedSubviews: [symbolLabel, fullNameLabel])
        labelStackView.distribution = .equalSpacing
        labelStackView.spacing = 8
        labelStackView.axis = .vertical
        labelStackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(labelStackView)
        
        NSLayoutConstraint.activate([
            labelStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.leadingPadding),
            labelStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            labelStackView.widthAnchor.constraint(equalToConstant: frame.width/2.2),
        ])
    }
    
    private func configurePriceLabels() {
        
        currentPriceLabel.text = "0"
        priceChangeLabel.text = "0.0%"
        
        let labelStackView = UIStackView(arrangedSubviews: [currentPriceLabel, priceChangeLabel])
        labelStackView.distribution = .equalSpacing
        labelStackView.spacing = 4
        labelStackView.axis = .vertical
        labelStackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(labelStackView)
        
        NSLayoutConstraint.activate([
            labelStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.leadingPadding),
            labelStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            labelStackView.widthAnchor.constraint(equalToConstant: frame.width/4.5)
        ])
    }
    
    private func configureChart() {
        addSubview(previewChart)
//        previewChart.backgroundColor = .white
        NSLayoutConstraint.activate([
//            previewChart.centerYAnchor.constraint(equalTo: centerYAnchor),
            previewChart.topAnchor.constraint(equalTo: topAnchor),
            previewChart.bottomAnchor.constraint(equalTo: bottomAnchor),
            previewChart.leadingAnchor.constraint(equalTo: centerXAnchor, constant: -30),
            //previewChart.trailingAnchor.constraint(equalTo: currentPriceLabel.leadingAnchor),
            //previewChart.heightAnchor.constraint(equalToConstant: contentView.bounds.height),
            previewChart.widthAnchor.constraint(equalToConstant: frame.width / 2.5)
        ])
    }
    
    func configure(with data: SymbolSearchResult) {
        symbolLabel.text = data.symbol
        fullNameLabel.text = data.description
    }
    
    func configureToMakeChart(with viewModel: ViewModel) {
    //func configureToMakeChart(with data: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []

        for (index, value) in viewModel.data.enumerated() {
            dataEntries.append(
                .init(
                    x: Double(index),
                    y: value))
        }
        
        let dataSet = LineChartDataSet(entries: dataEntries, label: "Some Label")
        dataSet.fillColor = .systemBlue
        dataSet.drawFilledEnabled = true
        dataSet.drawIconsEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.drawCirclesEnabled = false
        let data = LineChartData(dataSet: dataSet)
        previewChart.data = data
        
        previewChart.animate(yAxisDuration: 0.8, easingOption: .linear)
    }
    
    func configureToMakeCurrentPriceChange(with data: StockPriceForTheDay?) {
        guard let data = data,
              let percentChange = data.dp else { return }
        
        let percentChangeRoundedValue = round(percentChange * 100) / 100.0
        
        currentPriceLabel.text = "\(data.c)"
        priceChangeLabel.text = "\(percentChangeRoundedValue)%"
        
        priceChangeLabel.backgroundColor = percentChangeRoundedValue < 0 ? .systemRed : .systemGreen
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        symbolLabel.text = nil
        fullNameLabel.text = nil
        previewChart.data = nil
        currentPriceLabel.text = nil
        priceChangeLabel.text = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
