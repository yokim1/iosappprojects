//
//  File.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/10.
//

import UIKit

/// Tableview cell for search result
final class SearchResultTableViewCell: UITableViewCell {
    /// Identifier for cell
    static let identifier                   = "SearchResultTableViewCell"
    static let prefferedHeight: CGFloat     = 70
    
    private let symbolLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let fullNameLabel: UILabel = {
       let label = UILabel()
        label.textColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .black
        configureTitleLabels()
    }
    
    private func configureTitleLabels() {
        addSubview(symbolLabel)
        addSubview(fullNameLabel)
        
        symbolLabel.text = "symbol"
        fullNameLabel.text = "fullname"
        
        let labelStackView = UIStackView(arrangedSubviews: [symbolLabel, fullNameLabel])
        labelStackView.distribution = .equalSpacing
        labelStackView.spacing = 8
        labelStackView.axis = .vertical
        labelStackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(labelStackView)
        
        NSLayoutConstraint.activate([
            labelStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.leadingPadding),
            labelStackView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    func configure(with data: SymbolSearchResult) {
        symbolLabel.text    = data.displaySymbol
        fullNameLabel.text  = data.description
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
}
