//
//  CommunityTableViewCell.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/13.
//

import UIKit

class CommunityCommentTableViewCell: UITableViewCell {
    static let identifier = "CommunityCommentTableViewCell"
    static let prefferedHeight: CGFloat = 200

    let writerIdLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.textColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let titleLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bodyLabel: UILabel = {
       let label = UILabel()
        label.numberOfLines = 4
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let likeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "hand.thumbsup"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let commentsButton: UIButton = {
       let button = UIButton()
        button.setImage(UIImage(systemName: "bubble.right"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.isUserInteractionEnabled = false
        backgroundColor = .black
        configureUI()
    }
    
    private func configureUI() {
        
        writerIdLabel.text = "Writer"
        dateLabel.text = "2021.08.08 15:30"
        
        let nameDateLabelStackView = UIStackView(arrangedSubviews: [writerIdLabel, dateLabel])
        nameDateLabelStackView.spacing = 5
        nameDateLabelStackView.axis = .vertical
        nameDateLabelStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(nameDateLabelStackView)
        
        NSLayoutConstraint.activate([
            nameDateLabelStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            nameDateLabelStackView.topAnchor.constraint(equalTo: topAnchor)
        ])
        
        titleLabel.text = "Title"
        bodyLabel.text =
            "Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body"
        
        let titleBodyLabelStackView = UIStackView(arrangedSubviews: [titleLabel, bodyLabel])
        titleBodyLabelStackView.spacing = 5
        titleBodyLabelStackView.axis = .vertical
        titleBodyLabelStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleBodyLabelStackView)
        
        NSLayoutConstraint.activate([
            titleBodyLabelStackView.topAnchor.constraint(equalTo: nameDateLabelStackView.bottomAnchor),
            titleBodyLabelStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleBodyLabelStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
        
        let buttonStackView = UIStackView(arrangedSubviews: [likeButton, commentsButton])
        buttonStackView.distribution = .equalSpacing
        buttonStackView.spacing = 200
        buttonStackView.axis = .horizontal
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(buttonStackView)
        
        NSLayoutConstraint.activate([
//            buttonStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
//            buttonStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
//            buttonStackView.topAnchor.constraint(equalTo: titleBodyLabelStackView.bottomAnchor),
            
            buttonStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            buttonStackView.topAnchor.constraint(equalTo: titleBodyLabelStackView.bottomAnchor),
        ])
        
        likeButton.addTarget(self, action: #selector(likeButtonDidTapped), for: .touchUpInside)
        
    }
    
    @objc private func likeButtonDidTapped() {
        print("like Button")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
