//
//  MyWatchListCollectionViewCell.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/05.
//

import UIKit

struct StockDataForUserList {
    let marketChartData: CandleStick?
    let stockPriceForTheDay: StockPriceForTheDay?
}

class MyWatchListViewControllerCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "MyWatchListCollectionViewCell"
    
    let identity = "MyWatchListCollectionViewCell"
    
    var parent: BaseViewController?
    
//    fileprivate let tableView: UITableView = {
//        let tv = UITableView()
//        tv.backgroundColor = .black
//        tv.translatesAutoresizingMaskIntoConstraints = false
//        return tv
//    }()
//
//    private var stockChartMap: [String: CandleStick]          = [:]
//    private var stockPriceMap: [String: StockPriceForTheDay]  = [:]
//
//    //private var viewModels: [MyWatchListCell.ViewModel] = []
//
//    //    var data: [SymbolSearchResult] = []
//
//    var symbolSearchResult: [SymbolSearchResult] = []
//    //var marketChartData: [CandleStick] = []
//    //var stockPriceForTheDay: [StockPriceForTheDay] = []
//
//    var stockDataForUserList: [StockDataForUserList] = []
//
    
    lazy var vc: MyWatchListViewController = {
       let vc = MyWatchListViewController(delegate: parent)
        //vc.delegate = parent
        //parent?.addChild(vc)
        vc.didMove(toParent: parent)
        return vc
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
        
        //let uv = UIView()
        
//        let vc = MyWatchListViewController()
//        //uv.addSubview(vc.view)
//        parent?.addChild(vc)
//        vc.didMove(toParent: parent)
       // vc.delegate = parent
        addSubview(vc.view)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            vc.view.topAnchor.constraint(equalTo: topAnchor),
            vc.view.leadingAnchor.constraint(equalTo: leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: trailingAnchor),
            vc.view.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
//        fetchWatchlistData()
//        configureTableView()
    }
    
//    private func fetchWatchlistData() {
//        let group = DispatchGroup()
//
//        if symbolSearchResult.isEmpty {
//            group.enter()
//            PersistanceManager.shared.retrieveData { result in
//                defer{
//                    group.leave()
//                }
//                switch result {
//                case .success(let results):
//                    self.symbolSearchResult = results
//                    break
//
//                case .failure(let error):
//                    print(error)
//                    break
//                }
//            }
//        }
//
//        symbolSearchResult.forEach { symbolData in
//            group.enter()
//            NetworkManager.shared.fetchStockChartData(for: symbolData.symbol) {[weak self] result in
//                defer{
//                    group.leave()
//                }
//
//                switch result {
//                case .success(let data):
//
//                    //DispatchQueue.main.async {
//                    //                    self?.marketChartData.append(data)
//                    self?.stockChartMap[symbolData.symbol] = data
//                    //    self.tableView.reloadData()
//                    //}
//                    break
//
//                case .failure(let error):
//                    print(error)
//                    break
//                }
//            }
//
//            NetworkManager.shared.fetchStockPriceForTheDay(for: symbolData.symbol){[weak self] result in
//                //                defer {
//                //                    group.leave()
//                //                }
//
//                switch result {
//                case .success(let data):
//                    self?.stockPriceMap[symbolData.symbol] = data
//
//                    break
//
//                case .failure(let error):
//                    print(error)
//                    break
//                }
//            }
//        }
//
//        group.notify(queue: .main) {[weak self] in
//            print("update user interface")
//            self?.tableView.reloadData()
//        }
//
//    }
//
//    private func configureTableView() {
//        addSubview(tableView)
//        tableView.contentInset      = .init(top: 0, left: 0, bottom: frame.height/3, right: 0)
//        tableView.dataSource        = self
//        tableView.delegate          = self
//        tableView.rowHeight         = MyWatchListCell.prefferedHeight
//        tableView.register(MyWatchListCell.self, forCellReuseIdentifier: MyWatchListCell.identifier)
//
//        NSLayoutConstraint.activate([
//            tableView.topAnchor.constraint(equalTo: topAnchor),
//            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
//            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
//            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
//        ])
//    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
//
//extension MyWatchListViewControllerCollectionViewCell: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return symbolSearchResult.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyWatchListCell.identifier, for: indexPath) as? MyWatchListCell else {return UITableViewCell()}
//
//        if symbolSearchResult.isEmpty != true {
//            cell.configure(with: symbolSearchResult[indexPath.row])
//        }
//
//        //        if marketChartData.isEmpty == false {
//        //            cell.configureToMakeChart(with: .init(data: marketChartData[indexPath.row].c ?? []))
//        //            cell.configureToMakeCurrentPriceChange(with: stockPriceForTheDay[indexPath.row])
//        //        }
//
//        if stockChartMap.isEmpty == false{
//            cell.configureToMakeChart(with: .init(data: stockChartMap[symbolSearchResult[indexPath.row].symbol]?.c ?? []))
//            cell.configureToMakeCurrentPriceChange(with: stockPriceMap[symbolSearchResult[indexPath.row].symbol])
//        }
//
//        return cell
//    }
//}
//
//extension MyWatchListViewControllerCollectionViewCell: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//
//        let vc = DetailStockViewController(data: symbolSearchResult[indexPath.row])
//        //vc.delegate = self
//        parent?.navigationController?.pushViewController(vc, animated: true)
//    }
//}
//
//extension MyWatchListViewControllerCollectionViewCell: SearchResultsViewControllerDelegate {
//    func searchResultsViewControllerDidSelect(searchResult: SymbolSearchResult) {
//        let vc = DetailStockViewController(data: searchResult)
//        //vc.delegate = self
//        parent?.navigationController?.pushViewController(vc, animated: true)
//    }
//
//}
