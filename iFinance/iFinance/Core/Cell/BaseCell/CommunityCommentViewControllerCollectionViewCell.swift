//
//  CommunityCommentCollectionViewCell.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/05.
//

import UIKit

class CommunityCommentViewControllerCollectionViewCell: UICollectionViewCell {
    static let identifier = "CommunityCommentCollectionViewCell"
    
    let tableView: UITableView = {
       let tv = UITableView()
        tv.register(CommunityCommentTableViewCell.self, forCellReuseIdentifier: CommunityCommentTableViewCell.identifier)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.isUserInteractionEnabled = false
        configureTableView()
        
    }
    
    private func configureTableView() {
        addSubview(tableView)
        tableView.dataSource    = self
        tableView.delegate      = self
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CommunityCommentViewControllerCollectionViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommunityCommentTableViewCell.identifier, for: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        CommunityCommentTableViewCell.prefferedHeight
    }
}

extension CommunityCommentViewControllerCollectionViewCell: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}
