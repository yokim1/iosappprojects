//
//  CustomMenuBar.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/05.
//

import UIKit

class MenuTabBarView: UIView {
    
    let collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = .systemBlue
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    
    var baseViewController: BaseViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureCollectionView()
        configureHorizontalBar()
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
    }
    
    fileprivate func configureCollectionView() {
        collectionView.register(MenuBarCell.self, forCellWithReuseIdentifier: MenuBarCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    fileprivate func configureHorizontalBar() {
        let horizontalBar = UIView()
        horizontalBar.translatesAutoresizingMaskIntoConstraints = false
        horizontalBar.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        addSubview(horizontalBar)
        
        horizontalBarLeftAnchorConstraint = horizontalBar.leftAnchor.constraint(equalTo: self.leftAnchor)
        
        horizontalBarLeftAnchorConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
            horizontalBar.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            horizontalBar.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1 / CGFloat(Constants.numberOfItems)),
            horizontalBar.heightAnchor.constraint(equalToConstant: 4)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MenuTabBarView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Int(Constants.numberOfItems)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuBarCell.identifier, for: indexPath) as! MenuBarCell
        switch indexPath.item {
        case 0:
            cell.menuImageView.image = UIImage(systemName: "eye")
            break
            
        case 1:
            cell.menuImageView.image = UIImage(systemName: "message")
            break
            
        default:
            break
        }
        
        cell.backgroundColor = .black
        
        return cell
    }
}

extension MenuTabBarView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        baseViewController?.scrollToMenuIndex(menuIndex: indexPath.item)
    }
}

extension MenuTabBarView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (frame.width) / CGFloat(Constants.numberOfItems)
        return .init(width: width, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
}


class MenuBarCell: UICollectionViewCell {
    static let identifier = "MenuBarCell"
    
    let cellImageColor: UIColor = .darkGray
    
    let menuImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .darkGray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override var isHighlighted: Bool {
        didSet {
            menuImageView.tintColor = isHighlighted ? .white : cellImageColor
        }
    }
    
    override var isSelected: Bool {
        didSet {
            menuImageView.tintColor = isSelected ? .white : cellImageColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureCellImageView()
    }
    
    fileprivate func configureCellImageView() {
        addSubview(menuImageView)
        
        menuImageView.image?.withTintColor(.white)
        
        NSLayoutConstraint.activate([
            menuImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            menuImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            menuImageView.widthAnchor.constraint(equalToConstant: 25),
            menuImageView.heightAnchor.constraint(equalToConstant: 25),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
