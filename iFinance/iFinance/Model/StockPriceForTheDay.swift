//
//  StockPriceForTheDay.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/13.
//

import Foundation

struct StockPriceForTheDay: Codable {
    let c: Double
    let d: Double?
    let dp: Double?
    let h: Double
    let l: Double
    let o: Double
    let pc: Double
    let t: TimeInterval
}
