//
//  CandleStick.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/03.
//

import Foundation

struct CandleStick: Codable {
    let c: [Double]?
    let h: [Double]?
    let l: [Double]?
    let o: [Double]?
    let s: String
    let t: [TimeInterval]?
}
