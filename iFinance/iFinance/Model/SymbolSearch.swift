//
//  Stock.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/10.
//

import Foundation

struct SymbolSearchResponse: Codable {
    let count: Int
    let result: [SymbolSearchResult]
}

struct SymbolSearchResult: Codable, Equatable, Hashable {
    let description: String
    let displaySymbol: String
    let symbol: String
    let type:  String
}
