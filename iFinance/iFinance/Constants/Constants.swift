//
//  Constants.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/02.
//

import UIKit

struct Constants {
    static let numberOfItems: CGFloat = 2
    static let menuTabBarHeight: CGFloat = 50
    static let watchListHeight: CGFloat = 100
    static let leadingPadding: CGFloat = 20
    
    static let day: TimeInterval = 3600 * 24
}
