//
//  TableViewController.swift
//  SolarSystemAR
//
//  Created by 김윤석 on 2021/05/19.
//

import UIKit

protocol selectDelegate : AnyObject {
    func selectedPlanet(this planet: Planet)
}

class TableViewController: UITableViewController {

    //let planets = ["neptune", "uranus", "earth_daymap", "earth_nightmap", "jupiter", "mars", "mercury", "moon", "saturn", "sun", "venus_surface"]
    
    let planets: [Planet] = [
        Planet(name: "Sun", imageId: "Sun", textureId: "sun", radius: 0.8, revolutionSpeed: 200),
        
        Planet(name: "Jupiter", imageId: "Jupiter", textureId: "jupiter", radius: 0.5, revolutionSpeed: 25),
        Planet(name: "Saturn", imageId: "Saturn", textureId: "saturn", radius: 0.45, revolutionSpeed: 28),
        Planet(name: "Uranus", imageId: "Uranus", textureId: "uranus", radius: 0.3, revolutionSpeed: 40),
        Planet(name: "Neptune", imageId: "Neptune", textureId: "neptune", radius: 0.29, revolutionSpeed: 38),
       
        Planet(name: "Earth Day Time", imageId: "EarthDay", textureId: "earth_daymap", radius: 0.2, revolutionSpeed: 50),
        Planet(name: "Earth Night Time", imageId: "EarthNight", textureId: "earth_nightmap", radius: 0.2, revolutionSpeed: 50),
        
        Planet(name: "Venus", imageId: "Venus", textureId: "venus_surface", radius: 0.19, revolutionSpeed: 1000),
        Planet(name: "Mars", imageId: "Mars", textureId: "mars", radius: 0.17, revolutionSpeed: 55),
        Planet(name: "Mercury", imageId: "Mercury", textureId: "mercury", radius: 0.15, revolutionSpeed: 500),
        Planet(name: "Moon", imageId: "Moon", textureId: "moon", radius: 0.12, revolutionSpeed: 50),
        
    ]
    
    var selectedPlanet: Planet?
    
    var delegate: selectDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planets.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlanetInformationCell", for: indexPath) as? PlanetInformationCell else {return UITableViewCell()}
        
        if planets[indexPath.row].name == selectedPlanet?.name {
            cell.accessoryType = .checkmark
        } else{
            cell.accessoryType = .none
        }
        
        cell.planetImage.image = UIImage(named: planets[indexPath.row].imageId)
        cell.planetName.text = planets[indexPath.row].name
        cell.planetName.numberOfLines = 2
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        delegate?.selectedPlanet(this: planets[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}

class PlanetInformationCell: UITableViewCell {
    
    @IBOutlet weak var planetImage: UIImageView!
    @IBOutlet weak var planetName: UILabel!
}


struct Planet {
    let name: String
    let imageId: String
    let textureId: String
    let radius: CGFloat
    let revolutionSpeed: CFTimeInterval
}
