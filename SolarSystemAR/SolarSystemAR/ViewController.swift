//
//  ViewController.swift
//  SolarSystemAR
//
//  Created by 김윤석 on 2021/05/18.
//

import UIKit
import SceneKit
import ARKit

struct SelectedPlanetInformation {
    var textName: String?
    var actualName: String?
    
    var diameter: Float?
}

class ViewController: UIViewController, ARSCNViewDelegate, selectDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    
    var createdPlanets:[SCNNode] = []
    var selectedPlanet: Planet = Planet(name: "Earth Day Time", imageId: "Earth", textureId: "earth_daymap", radius: 0.1, revolutionSpeed: 50)
    
    //MARK: -Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        sceneView.autoenablesDefaultLighting = true
        
        self.navigationController?.title = "Solar"
        
        // Declaring Saturn Ring Using Torus
        //let saturnusRingTorus = SCNTorus(ringRadius: 0.1, pipeRadius: 0.01)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            
            //let touchLocation = touch.location(in: sceneView)
            
            //let resultsVertical = sceneView.hitTest(touchLocation, types: .estimatedVerticalPlane)
            
            let location = touch.location(in: sceneView)
            guard let query = sceneView.raycastQuery(from: location, allowing: .estimatedPlane, alignment: .any) else {
                return
            }
            
            let results = sceneView.session.raycast(query)
            make3DPlanetModel(results, radius: selectedPlanet.radius, revolutionSpeed: selectedPlanet.revolutionSpeed)
        }
    }
    
    func make3DPlanetModel(_ resultsVertical: [ARRaycastResult], radius: CGFloat, revolutionSpeed: CFTimeInterval) {
        if let hitResult = resultsVertical.first {
            let sphere = SCNSphere(radius: radius)
            
            let material = SCNMaterial()
            
            material.diffuse.contents = UIImage(named: "art.scnassets/\(selectedPlanet.textureId).jpg")
            
            sphere.materials = [material]
            
            let node = SCNNode()
            
            node.position = SCNVector3(
                hitResult.worldTransform.columns.3.x,
                hitResult.worldTransform.columns.3.y + Float(sphere.radius),
                hitResult.worldTransform.columns.3.z )
            
            node.geometry = sphere
            
            //node.opacity = 0.5
            
            sceneView.scene.rootNode.addChildNode(node)
            
            createRing(hitResult: hitResult, sphere: sphere)
            createAtmosphere(hitResult: hitResult, sphere: sphere)
            
            //For Rotating animation
            let spin = CABasicAnimation(keyPath: "rotation")
            spin.fromValue = NSValue(scnVector4: SCNVector4(x: 0, y: 1, z: 0, w: 0))
            spin.toValue = NSValue(scnVector4: SCNVector4(x: 0, y: 1, z: 0, w: Float(CGFloat(2 * Double.pi))))
            spin.duration = revolutionSpeed
            spin.repeatCount = .infinity
            node.addAnimation(spin, forKey: "spin around")
            
            createdPlanets.append(node)
        }
    }
    
    // MARK: - ARSCNViewDelegate
    
    /*
     // Override to create and configure nodes for anchors added to the view's session.
     func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     let node = SCNNode()
     
     return node
     }
     */
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    //MARK: - Buttons
    
    @IBAction func deleteAllButtonDidTapped(_ sender: Any) {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Delete All?", message: "You want to delete all planets?", preferredStyle: .actionSheet)
        
        let deleteActionButton = UIAlertAction(title: "Delete", style: .destructive) { _ in
            if !self.createdPlanets.isEmpty {
                for planet in self.createdPlanets {
                    planet.removeFromParentNode()
                }
            }
        }
        actionSheetController.addAction(deleteActionButton)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .default){_ in }
        actionSheetController.addAction(cancelActionButton)

        if UIDevice.current.userInterfaceIdiom == .pad { //디바이스 타입이 iPad일때
            if let popoverController = actionSheetController.popoverPresentationController { // ActionSheet가 표현되는 위치를 저장해줍니다.
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func selectPlanetButtonDidTapped(_ sender: Any) {
        guard let selectPlanetViewController = storyboard?.instantiateViewController(identifier: "TableViewController") as? TableViewController else {return }
        selectPlanetViewController.delegate = self
        //self.navigationController?.pushViewController(selectPlanetViewController, animated: true)
        selectPlanetViewController.selectedPlanet = selectedPlanet
        present(selectPlanetViewController, animated: true, completion: nil)
    }
    
    func selectedPlanet(this planet: Planet) {
        selectedPlanet = planet
    }
    
    func createRing(hitResult: ARRaycastResult, sphere: SCNSphere){
        if selectedPlanet.name == "Saturn" {
            let saturnusRingTorus = SCNTube(innerRadius: selectedPlanet.radius + 0.2, outerRadius: selectedPlanet.radius + 0.6, height: 0.001)
            
            // Declaring Materials & Textures
            let saturnusRingMaterial = SCNMaterial()
            
            saturnusRingMaterial.diffuse.contents = UIImage(named: "textures.scnassets/saturn_ring_alpha.png")
            saturnusRingTorus.materials = [saturnusRingMaterial]
            
            // Show Ring Through Node
            let nodeSaturnusRing = SCNNode()
            nodeSaturnusRing.position = SCNVector3(
                hitResult.worldTransform.columns.3.x,
                hitResult.worldTransform.columns.3.y + Float(sphere.radius),
                hitResult.worldTransform.columns.3.z )
            
            nodeSaturnusRing.geometry = saturnusRingTorus
            nodeSaturnusRing.rotation = SCNVector4(0, 0, 1, Float.pi)
            
            sceneView.scene.rootNode.addChildNode(nodeSaturnusRing)
            createdPlanets.append(nodeSaturnusRing)
        }
    }
    
    func createAtmosphere(hitResult: ARRaycastResult, sphere: SCNSphere){
        if selectedPlanet.name == "Earth Day Time" || selectedPlanet.name == "Earth Night Time" {
            let skySphere = SCNSphere(radius: selectedPlanet.radius + 0.0018)
            
            let material = SCNMaterial()
            
            material.diffuse.contents = UIImage(named: "art.scnassets/earth_clouds.jpg")
            
            skySphere.materials = [material]
            
            let skyNode = SCNNode()
            skyNode.position = SCNVector3 (
                hitResult.worldTransform.columns.3.x,
                hitResult.worldTransform.columns.3.y + Float(sphere.radius),
                hitResult.worldTransform.columns.3.z )
            
            skyNode.geometry = skySphere
            
            skyNode.opacity = 0.5
            
            sceneView.scene.rootNode.addChildNode(skyNode)
            
            let spin = CABasicAnimation(keyPath: "rotation")
            spin.fromValue = NSValue(scnVector4: SCNVector4(x: 0, y: 1, z: 0, w: 0))
            spin.toValue = NSValue(scnVector4: SCNVector4(x: 0, y: 1, z: 0, w: Float(CGFloat(2 * Double.pi))))
            spin.duration = selectedPlanet.revolutionSpeed - 10
            spin.repeatCount = .infinity
            skyNode.addAnimation(spin, forKey: "spin around")
            
            createdPlanets.append(skyNode)
        }
        else if selectedPlanet.name == "Venus" {
            let skySphere = SCNSphere(radius: selectedPlanet.radius + 0.0018)
            
            let material = SCNMaterial()
            
            material.diffuse.contents = UIImage(named: "art.scnassets/venus_atmosphere.jpg")
            
            skySphere.materials = [material]
            
            let skyNode = SCNNode()
            
            skyNode.position = SCNVector3 (
                hitResult.worldTransform.columns.3.x,
                hitResult.worldTransform.columns.3.y + Float(sphere.radius),
                hitResult.worldTransform.columns.3.z )
            
            skyNode.geometry = skySphere
            
            skyNode.opacity = 0.5
            
            sceneView.scene.rootNode.addChildNode(skyNode)
            
            let spin = CABasicAnimation(keyPath: "rotation")
            spin.fromValue = NSValue(scnVector4: SCNVector4(x: 0, y: 1, z: 0, w: 0))
            spin.toValue = NSValue(scnVector4: SCNVector4(x: 0, y: 1, z: 0, w: Float(CGFloat(2 * Double.pi))))
            spin.duration = 100
            spin.repeatCount = .infinity
            skyNode.addAnimation(spin, forKey: "spin around")
            
            createdPlanets.append(skyNode)
        }
    }
}
