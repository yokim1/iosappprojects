# iOSAppProjects

This directory is for my apps that will be in App Store


## Adrenalist

I built this app just for my own workout.
The core purpose of this app is:  
                                - Save user's time that can be wasted 
                                - Track user's past workout so that the user can build their own strategy for their future workout


## Solar System AR

I built this app to practice AR and for people who loves to observe planets.


## iFinance
