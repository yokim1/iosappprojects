//
//  iFinance2Tests.swift
//  iFinance2Tests
//
//  Created by 김윤석 on 2021/10/03.
//

import XCTest
@testable import iFinance2

class iFinance2Tests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
        print("class level set up")
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        print("set up")
    }
    
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        print("tear down")
    }
    
    override class func tearDown() {
        super.tearDown()
        print("class level tear down")
    }
    
    func testHomeViewModel() throws {
        //Arrange
        let hv = HomeViewModel()
        
        //Act
        hv.nice()
        
        //Assert
        XCTAssertEqual("nice!", "nice")
    }

    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        print("Test")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
        
        print("Test Performance")
    }
}
