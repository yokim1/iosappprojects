//
//  StockCharModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import UIKit

struct StockChartModel {
    let data: [Double]
    let showLegend: Bool
    let showAxis: Bool
    let fillColor: UIColor
    let isFillColor: Bool
}
