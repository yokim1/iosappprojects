//
//  SearchViewController.swift
//  iFinance
//
//  Created by 김윤석 on 2021/08/02.
//

import UIKit

class SearchViewController: UITableViewController{

    //MARK: - ViewModel
    
    private let viewModel: SearchViewModel?
    
    //MARK: - Init
    
    init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        extendedLayoutIncludesOpaqueBars = true
        viewModel?.delegate = self
        
        configureTableView()
        configureNavigationBar()
        configureSearchBar()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - SearchViewModel Delegate

extension SearchViewController: SearchViewModelDelegate {
    func didSelectSymbolToOpen(_ viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}

//MARK: - Configure UI

extension SearchViewController {
    
    private func configureTableView() {
        tableView.delegate = viewModel
        viewModel?.configureDataSource(for: tableView)
        
        tableView.backgroundColor = .systemBackground
        tableView.rowHeight         = SearchResultTableViewCell.prefferedHeight
        tableView.register(SearchResultTableViewCell.self, forCellReuseIdentifier: SearchResultTableViewCell.identifier)
    }
    
    private func configureNavigationBar() {
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = .white
    }
    
    private func configureSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = viewModel
        
        navigationItem.searchController = searchController
        searchController.searchBar.tintColor = .white
        searchController.searchBar.searchTextField.textColor = .white
        searchController.obscuresBackgroundDuringPresentation = false
        
        navigationItem.hidesSearchBarWhenScrolling = false
    }
}
