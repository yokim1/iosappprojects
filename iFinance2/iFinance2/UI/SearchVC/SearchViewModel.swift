//
//  SearchViewModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import UIKit

protocol SearchViewModelDelegate: AnyObject {
    func didSelectSymbolToOpen(_ viewController: UIViewController)
}

class SearchViewModel: NSObject {
    
    enum Section {
        case first
    }
    
    weak var delegate: SearchViewModelDelegate?
    
    //MARK: - Properties
    
    private var data: [SearchResult] = []
    private var dataSource: UITableViewDiffableDataSource<Section, SearchResult>?
    private var searchTimer = Timer()
}

//MARK: - Fetch Data

extension SearchViewModel {
    private func fetchData(query: String) {
        // Reset timer
        searchTimer.invalidate()
        
        // Kick off new timer
        // Optimize to reduce number of searches for when user stops typing
        searchTimer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { _ in
            APICaller.shared.search(query: query) { result in
                switch result {
                case .success(let response):
                    DispatchQueue.main.async {
                        self.data = response.result
                        self.updateDataSource()
                    }
                    break
                    
                case .failure(_):
                    DispatchQueue.main.async {
                        self.data = []
                        self.updateDataSource()
                    }
                    break
                }
            }
        })
    }
}

//MARK: - TableView Delegate

extension SearchViewModel: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = data[indexPath.row]
        let vc = StockDetailsViewController(symbol: data.symbol, companyName: data.description)
        delegate?.didSelectSymbolToOpen(vc)
    }
}

//MARK: - Diffable TableView Data Source
extension SearchViewModel {
    func configureDataSource(for tableView: UITableView) {
        dataSource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { tableView, indexPath, model -> UITableViewCell in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.identifier, for: indexPath) as! SearchResultTableViewCell
            cell.textLabel?.text = model.symbol
            cell.detailTextLabel?.text = model.description
            return cell
        })
        
        dataSource?.defaultRowAnimation = .middle
    }
    
    private func updateDataSource() {
        var snapShot = NSDiffableDataSourceSnapshot<Section, SearchResult>()
        snapShot.appendSections([.first])
        snapShot.appendItems(self.data)
        
        dataSource?.apply(snapShot, animatingDifferences: true, completion: nil)
    }
}

//MARK: - Update Search Result with Input Text

extension SearchViewModel: UISearchResultsUpdating {
    /// Update search on key tap
    /// - Parameter searchController: Ref of the search controlelr
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text,
              !query.trimmingCharacters(in: .whitespaces).isEmpty else {
                  return
              }
        fetchData(query: query)
    }
}
