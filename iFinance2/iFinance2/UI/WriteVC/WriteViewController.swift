//
//  WriteViewController.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/08/25.
//
import FirebaseDatabase
import UIKit

protocol WriterViewControllerDelegate: AnyObject {
    func saveButtonDidTap(_ postContent: PostContent, _ vc: WriteViewController)
}

class WriteViewController: UIViewController {

    //MARK: - Delegate
    
    weak var delegate: WriterViewControllerDelegate?
    
    //MARK: - UI Objects
    
    private let symbol: String?

    private lazy var tableView: UITableView = {
       let tableView = UITableView()
        tableView.register(WriteTableViewCell.self, forCellReuseIdentifier: WriteTableViewCell.identifier)
        tableView.rowHeight = view.frame.height
        tableView.isUserInteractionEnabled = true
        tableView.allowsSelection = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    //MARK: - ViewModel
    
    private let viewModel = WriteViewModel()
    
    //MARK: - Init
    
    init(symbol: String? = nil) {
        self.symbol = symbol
        super.init(nibName: nil, bundle: nil)
    }
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        title = "Community"
        
        setUpViewModel()
        setupNavigationLeftBarItems()
        setupTableView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - SetUp View Model

extension WriteViewController {
    
    private func setUpViewModel() {
        viewModel.symbol = symbol
        viewModel.delegate = self
    }
}

//MARK: - WriteViewModel Delegate

extension WriteViewController: WriteViewModelDelegate {
    
    func saveButtonDidTap() {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - Button Action
extension WriteViewController {
    
    @objc private func closeButtonDidTap() {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - SetUp UI

extension WriteViewController {
    
    private func setupNavigationLeftBarItems() {
        let closeButton = UIBarButtonItem(image: UIImage(systemName: "xmark.circle"), style: .done, target: self, action: #selector(closeButtonDidTap))
        closeButton.tintColor = .white
        navigationItem.leftBarButtonItems = [closeButton]
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.dataSource = viewModel
    }
}
