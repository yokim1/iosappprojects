//
//  WriteViewModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import UIKit

protocol WriteViewModelDelegate: AnyObject {
    func saveButtonDidTap()
}

class WriteViewModel: NSObject {
    
    var symbol: String?
    weak var delegate: WriteViewModelDelegate?
}

//MARK: - WriteTableViewCell Delegate
extension WriteViewModel: WriteTableViewCellDelegate {
    func saveButtonDidTap() {
        delegate?.saveButtonDidTap()
    }
}

//MARK: - UITableView DataSource

extension WriteViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WriteTableViewCell.identifier, for: indexPath) as? WriteTableViewCell else {return UITableViewCell()}
        cell.symbol = symbol
        cell.delegate = self
        return cell
    }
}
