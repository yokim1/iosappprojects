//
//  WriteTableViewCell.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/08/28.
//

import Firebase
import UIKit

protocol WriteTableViewCellDelegate: AnyObject {
    func saveButtonDidTap()
}

class WriteTableViewCell: UITableViewCell {

    static let prefferedHeight: CGFloat = 300
    static let identifier = "WriteTableViewCell"

    var delegate: WriteTableViewCellDelegate?
    
//    var type: String?
    
    lazy var writerIdTextField: UITextField = {
       let textField = UITextField()
        textField.backgroundColor = .tertiaryLabel
        textField.layer.cornerRadius = 8
        textField.addLeftPadding()
        textField.placeholder = "ID"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    lazy var titleTextField: UITextField = {
       let textField = UITextField()
        textField.backgroundColor = .tertiaryLabel
        textField.layer.cornerRadius = 8
        textField.addLeftPadding()
        textField.placeholder = "Title"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    lazy var bodyTextField: UITextField = {
       let textField = UITextField()
        textField.backgroundColor = .tertiaryLabel
        textField.layer.cornerRadius = 8
        textField.addLeftPadding()
        textField.placeholder = "Share your idea or opinion"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let bodyTextView = CaptionTextView()
    
    lazy var saveButton: UIButton = {
       let button = UIButton()
        button.backgroundColor = ConstantsColor.themeColor
        button.setTitle("SAVE", for: .normal)
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(saveButtonDidTap), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var symbol:String?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: nil)
        setupTitleTextField()
        setupBodyTextField()
        setupButtons()
    }
    
    private func setupTitleTextField() {
        
        contentView.addSubviews(writerIdTextField, titleTextField)
        
        NSLayoutConstraint.activate([
            writerIdTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            writerIdTextField.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 10),
            writerIdTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            writerIdTextField.heightAnchor.constraint(equalToConstant: 40),
            
            titleTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleTextField.topAnchor.constraint(equalTo: writerIdTextField.bottomAnchor, constant: 10),
            titleTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            titleTextField.heightAnchor.constraint(equalToConstant: 40),
        ])
    }

    private func setupBodyTextField() {
        contentView.addSubview(bodyTextView)
        bodyTextView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            bodyTextView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            bodyTextView.topAnchor.constraint(equalTo: titleTextField.bottomAnchor, constant: 20),
            bodyTextView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            bodyTextView.heightAnchor.constraint(equalToConstant: 200),
        ])
    }
    
    private func setupButtons() {
        
        contentView.addSubview(saveButton)
        
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: bodyTextView.bottomAnchor, constant: 20),
            saveButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveButton.heightAnchor.constraint(equalToConstant: 40),
            saveButton.widthAnchor.constraint(equalToConstant: frame.width / 4)
        ])
    }
    
    let database = Database.database().reference()
    
    @objc func saveButtonDidTap() {
        
        guard let symbol = symbol,
              let idString = writerIdTextField.text,
              let titleString = titleTextField.text,
              let bodyString = bodyTextView.text else { return }
        
        let values = ["id": idString,
                      "title": titleString,
                      "date": Int(NSDate().timeIntervalSince1970),
                      "body": bodyString] as [String : Any]
        
        database.child("specificTalk").child(symbol).childByAutoId().updateChildValues(values) { error, ref in
            //let postContent = PostContent(id: idString, title: titleString, body: bodyString)
            //self.delegate?.saveButtonDidTap(postContent, self)
        }
        
        delegate?.saveButtonDidTap()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
