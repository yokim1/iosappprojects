//
//  MenuBarView.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/09/26.
//

import UIKit

protocol MenuBarViewDelegate: AnyObject {
    func didSelectItemAt(index: Int)
}

class MenuBarView: UIView {
    
    private let myListButton: MenuBarButton
    private let opinionsButton: MenuBarButton
    private var buttons: [UIButton] = []
    
    weak var delegate: MenuBarViewDelegate?
    
    override init(frame: CGRect) {
        myListButton = MenuBarButton(title: "My List")//makeButton(withText: "My List")
        opinionsButton = MenuBarButton(title: "Opinions")//makeButton(withText: "Opinions")
        buttons = [myListButton, opinionsButton]
        super.init(frame: .zero)
        
        configureButtons()
        configureMenuBarButtons()
    }
    
    private func configureButtons() {
        myListButton.addTarget(self, action: #selector(playlistsButtonTapped), for: .primaryActionTriggered)
        opinionsButton.addTarget(self, action: #selector(artistsButtonTapped), for: .primaryActionTriggered)

        setAlpha(for: myListButton)
    }
    
    private func configureMenuBarButtons() {
        
        let leadPadding: CGFloat = 16
        let buttonSpace: CGFloat = 36

        addSubviews(myListButton, opinionsButton)//, albumsButton )//indicator)
        
        NSLayoutConstraint.activate([
            // Buttons
            myListButton.topAnchor.constraint(equalTo: topAnchor),
            myListButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leadPadding),
            myListButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 10),
            
            opinionsButton.topAnchor.constraint(equalTo: topAnchor),
            opinionsButton.leadingAnchor.constraint(equalTo: myListButton.trailingAnchor, constant: buttonSpace),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Button Actions

extension MenuBarView {
    
    @objc private func playlistsButtonTapped() {
        delegate?.didSelectItemAt(index: 0)
    }
    
    @objc private func artistsButtonTapped() {
        delegate?.didSelectItemAt(index: 1)
    }
}

extension MenuBarView {
    
    func selectItem(at index: Int) {
         animateIndicator(to: index)
    }
    
    func scrollIndicator(to contentOffset: CGPoint) {
        let index = Int(contentOffset.x / frame.width)
        setAlpha(for: buttons[index])
    }
    
    private func setAlpha(for button: UIButton) {
            self.myListButton.alpha = 0.5
            self.opinionsButton.alpha = 0.5
            button.alpha = 1.0
    }
    
    private func animateIndicator(to index: Int) {

        var button: UIButton
        
        switch index {
        case 0:
            button = myListButton
        case 1:
            button = opinionsButton
        default:
            button = myListButton
        }
        
        setAlpha(for: button)
    }
}
