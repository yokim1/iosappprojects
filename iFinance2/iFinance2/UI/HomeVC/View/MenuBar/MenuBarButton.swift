//
//  MenuBarButton.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import UIKit

class MenuBarButton: UIButton {
    init(title: String){
//    override init(frame: CGRect) {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        setTitle(title, for: .normal)
        titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
