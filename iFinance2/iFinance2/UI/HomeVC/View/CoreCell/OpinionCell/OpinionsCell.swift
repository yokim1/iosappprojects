//
//  OpinionsCell.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/09/26.
//
//import FirebaseDatabase
import UIKit

protocol OpinionsCellDelegate: AnyObject {
    func opinionDidSelect()
}

class OpinionsCell: UICollectionViewCell {
    
    static let identifier = "OpinionsCell"
    
    //MARK: - Model
    private var postContents: [PostContent] = [ ]
    
    //MARK: - ViewModel
    private let viewModel: OpinionsViewModel
    
    //MARK: - UI Objects
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.layer.cornerRadius = 16
        tableView.layer.masksToBounds = true
        tableView.register(CommentTableViewCell.self, forCellReuseIdentifier: CommentTableViewCell.identifier)
        tableView.delegate = viewModel
        tableView.dataSource = viewModel
        tableView.allowsSelection = false
        
        return tableView
    }()
    
    weak var delegate: OpinionsCellDelegate?
    
    //MARK: - Init
    override init(frame: CGRect) {
        self.viewModel = OpinionsViewModel()
        super.init(frame: frame)
        viewModel.delegate = self
        configureTableView()
    }
    
    private func configureTableView() {
        
        contentView.addSubviews(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Opinions Delegate
extension OpinionsCell: OpinionsDelegate {
    func didselectRowAt(indexPath: IndexPath) {
        delegate?.opinionDidSelect()
    }
    
    func updateTableView() {
        tableView.reloadData()
    }
}

