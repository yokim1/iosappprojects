//
//  OpinionViewModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import FirebaseDatabase
import UIKit

protocol OpinionsDelegate: AnyObject {
    func didselectRowAt(indexPath: IndexPath)
    func updateTableView()
}

class OpinionsViewModel: NSObject{
    //MARK: - Model
    private var postContents: [PostContent] = []
    
    weak var delegate: OpinionsDelegate?
    
    //MARK: - Init
    override init() {
        super.init()
        fetchComments()
    }
    
    private let database = Database.database().reference().child("specificTalk")
    
    private func fetchComments() {
        let symbol = "generalTalk"
        database.child(symbol).observe(.childAdded) { snapShot in
            guard let dictionary = snapShot.value as? [String: Any] else { return }

            guard let idString = dictionary["id"] as? String,
                  let titleString = dictionary["title"] as? String,
                  let date = dictionary["date"] as? Double,
                  let bodyString = dictionary["body"] as? String else { return }
            
            let postContent = PostContent(id: idString,
                                          title: titleString,
                                          date: Date(timeIntervalSince1970: date),
                                          body: bodyString)
            
            self.postContents.append(postContent)
            
            DispatchQueue.main.async {
                self.postContents.sort { postContentA, postContentB in
                    postContentA.date > postContentB.date
                }
                self.delegate?.updateTableView()
            }
        }
    }
}

//MARK: - TableView Data Source
extension OpinionsViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        postContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.identifier, for: indexPath) as? CommentTableViewCell else {return UITableViewCell()}
        cell.configure(with: postContents[indexPath.row])
        return cell
    }
}

//MARK: - TableView Delegate
extension OpinionsViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didselectRowAt(indexPath: indexPath)
    }
}
