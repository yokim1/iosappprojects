//
//  AssetListCell.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/09/26.
//
import FloatingPanel
import UIKit


protocol WatchListCellDelegate: AnyObject {
    func myListDidSelect(toPresent viewController: UIViewController)
}

class MyListCell: UICollectionViewCell {
    
    static let identifier = "WatchListCell"
    
    private let viewModel: MyListViewModel
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.layer.cornerRadius = 16
        tableView.layer.masksToBounds = true
        tableView.register(WatchListTableViewCell.self, forCellReuseIdentifier: WatchListTableViewCell.identifier)
        tableView.dataSource = viewModel
        tableView.delegate = viewModel
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: contentView.frame.height/3, right: 0)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    weak var delegate: WatchListCellDelegate?
    
    override init(frame: CGRect) {
        self.viewModel = MyListViewModel()
        super.init(frame: frame)
        
        viewModel.delegate = self
        setupTableView()
    }
    
    @objc private func refreshed() {
        viewModel.fetchWatchlistData()
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    let refreshControl = UIRefreshControl()
    
    private func setupTableView() {
        addSubviews(tableView)
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(refreshed), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MyListCell: MyListViewModelDelegate {
    
    func updateTableView() {
//        viewModel.fetchWatchlistData()
        tableView.reloadData()
    }
    
    func myListDidSelect(toPresent viewController: UIViewController) {
        delegate?.myListDidSelect(toPresent: viewController)
    }
    
}
