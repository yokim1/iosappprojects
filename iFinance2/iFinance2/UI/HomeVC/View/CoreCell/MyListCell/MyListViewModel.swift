//
//  MyListViewModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import UIKit

protocol MyListViewModelDelegate: AnyObject {
    func updateTableView()
    func myListDidSelect(toPresent viewController: UIViewController)
}

class MyListViewModel: NSObject {
    
    //MARK: - Model
    
    private var watchlistChartMap: [String: [CandleStick]] = [:]
    private var watchlistQuoteMap: [String: Quote] = [:]
    
    //MARK: - ViewModel
    
    private var myWatchStocks: [MyWatchListModel] = []
    
    /// Delegate
    weak var delegate: MyListViewModelDelegate?
    
    /// Observer for watch list updates
    private var observer: NSObjectProtocol?
    
    //MARK: - Init
    
    override init() {
        super.init()
        
        fetchWatchlistData()
        setUpObserver()
    }
}

extension MyListViewModel {
    /// Fetch watch list models
    func fetchWatchlistData() {
        let symbols = PersistenceManager.shared.watchlist
        
        createPlaceholderForLoadingMyWatchStock()
        
        let group = DispatchGroup()
        
//        for symbol in symbols where watchlistQuoteMap[symbol] == nil {
            
        symbols.forEach { symbol in
            
            group.enter()
            APICaller.shared.marketData(for: symbol) { [weak self] result in
                defer {
                    group.leave()
                }
                
                switch result {
                case .success(let data):
                    let candleSticks = data.candleSticks
                    self?.watchlistChartMap[symbol] = candleSticks
                case .failure(let error):
                    print(error)
                }
            }
            
            group.enter()
            APICaller.shared.quote(for: symbol) { [weak self] result in
                defer {
                    group.leave()
                }
                
                switch result {
                case .success(let quote):
                    self?.watchlistQuoteMap[symbol] = quote
                case .failure(let error):
                    print(error)
                }
            }
        }
        
        group.notify(queue: .main) { [weak self] in
            self?.createMyWatchStocks()
//            self?.tableView.reloadData()
            self?.delegate?.updateTableView()
        }
    }
    
    /// Sets up observer for watch list updates
    private func setUpObserver() {
        observer = NotificationCenter.default.addObserver( forName: .didAddToWatchList, object: nil, queue: .main) { [weak self] _ in
            self?.myWatchStocks.removeAll()
            self?.fetchWatchlistData()
        }
    }
    
    /// Creates view models from models
    private func createMyWatchStocks() {
        var viewModels = [MyWatchListModel]()
        
        for (symbol, candleSticks) in watchlistChartMap {
            let changePercentage = watchlistQuoteMap[symbol]?.percentChange
            
            viewModels.append(
                .init(
                    symbol: symbol,
                    companyName: UserDefaults.standard.string(forKey: symbol) ?? "Company",
                    price: getLatestClosingPrice(from: candleSticks),
                    changeColor: changePercentage ?? 0.0 < 0 ? .systemRed : .systemGreen,
                    changePercentage: .percentage(from: changePercentage ?? 0.0),
                    chartViewModel: .init(
                        data: candleSticks.reversed().map { $0.close },
                        showLegend: false,
                        showAxis: false,
                        fillColor: changePercentage ?? 0.0 < 0 ? .systemRed : .systemGreen,
                        isFillColor: false
                    )
                )
            )
        }
        
        self.myWatchStocks = viewModels.sorted(by: { $0.symbol < $1.symbol })
    }
    
    private func createPlaceholderForLoadingMyWatchStock() {
        let symbols = PersistenceManager.shared.watchlist
        symbols.forEach { _ in
            myWatchStocks.append(
                .init(symbol: "Loading", companyName: "...",
                      price: "Loding", changeColor: .darkGray, changePercentage: "...",
                      chartViewModel: .init( data: [], showLegend: false, showAxis: false, fillColor: .clear, isFillColor: false)
                     )
            )
        }
//        self.myWatchStocks = myWatchStocks.sorted(by: { $0.symbol < $1.symbol })
        self.delegate?.updateTableView()
    }
    
    /// Gets latest closing price
    /// - Parameter data: Collection of data
    /// - Returns: String
    private func getLatestClosingPrice(from data: [CandleStick]) -> String {
        guard let closingPrice = data.first?.close else { return ""}
        return .formatted(number: closingPrice)
    }
}

//MARK: - Tableview Data Source

extension MyListViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myWatchStocks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell( withIdentifier: WatchListTableViewCell.identifier, for: indexPath) as? WatchListTableViewCell else {
            fatalError()
        }
//        cell.delegate = self
        cell.configure(with: myWatchStocks[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tableView.beginUpdates()
            
            // Update persistence
            PersistenceManager.shared.removeFromWatchlist(symbol: myWatchStocks[indexPath.row].symbol)
            
            // Update viewModels
            myWatchStocks.remove(at: indexPath.row)
            
            // Delete Row
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            tableView.endUpdates()
        }
    }
}

//MARK: - UITableView Delegate
extension MyListViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return WatchListTableViewCell.preferredHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //MARK: - DELEGATE THIS
//        HapticsManager.shared.vibrateForSelection()
//
        let viewModel = myWatchStocks[indexPath.row]
        let vc = StockDetailsViewController (
            symbol: viewModel.symbol,
            companyName: viewModel.companyName,
        
            candleStickData: watchlistChartMap[viewModel.symbol] ?? [])
        
        delegate?.myListDidSelect(toPresent: vc)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
}
