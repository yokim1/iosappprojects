//
//  HomeController.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/08/25.
//

import FloatingPanel
import UIKit

class HomeController: UIViewController {
    
    //MARK: - UI Objects
    
    private let menuBar = MenuBarView()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        
        cv.register(MyListCell.self, forCellWithReuseIdentifier: MyListCell.identifier)
        cv.register(OpinionsCell.self, forCellWithReuseIdentifier: OpinionsCell.identifier)
        cv.backgroundColor = .systemGray3
        cv.isPagingEnabled = true
        cv.dataSource = viewModel
        cv.delegate = viewModel
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    private lazy var searchButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .done, target: self, action: #selector(searchButtonDidTap))
    
    private lazy var writeOpinionsButton = UIBarButtonItem(image: UIImage(systemName: "square.and.pencil"), style: .done, target: self, action: #selector(writeOpinionButtonDidTap))
    
    //MARK: - Life Cycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        navigationController?.navigationItem.hidesBackButton = true
        navigationItem.hidesBackButton = true
        setUpTitleView()
        setupNavigationRightBarItems()
        configureMenuBar()
        configureCollectionView()
        setUpFloatingPanel()
    }
    
    //MARK: - ViewModel
    
    private let viewModel: HomeViewModel
    
    //MARK: - Init

    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - HomeViewModel Delegate

extension HomeController: HomeViewModelDelegate {
    func scrollToItem(at indexPath: IndexPath, at scrollPosition: UICollectionView.ScrollPosition, animated: Bool) {
        collectionView.scrollToItem(at: indexPath, at: scrollPosition, animated: animated)
    }
    
    func changeNavigationBarButtonStatus(alpha: CGFloat, isEnabled: Bool) {
        navigationItem.titleView?.alpha = alpha
        
        searchButton.tintColor?.withAlphaComponent(alpha)
        searchButton.isEnabled = isEnabled
        
        writeOpinionsButton.tintColor?.withAlphaComponent(alpha)
        writeOpinionsButton.isEnabled = isEnabled
    }
    
    func myListDidSelect(toPresent viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func scrollIndicator(to scrollView: UIScrollView) {
        menuBar.scrollIndicator(to: scrollView.contentOffset)
    }

    func cellSizeForHomeControllerCell() -> CGSize {
        CGSize(width: view.frame.width, height: collectionView.frame.height)
    }
}

//MARK: - Button Actions

extension HomeController {
    
    @objc private func writeOpinionButtonDidTap() {
        let vc = WriteViewController(symbol: "generalTalk")
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
    @objc private func searchButtonDidTap() {
        let vc = SearchViewController(viewModel: SearchViewModel())
        navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - Setup UI

extension HomeController {
    
    private func setUpTitleView() {
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: navigationController?.navigationBar.height ?? 100))
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: titleView.width-20, height: titleView.height))
        label.text = "iFinance"
        label.font = .systemFont(ofSize: 30, weight: .medium)
        titleView.addSubview(label)
        
        navigationItem.titleView = titleView
    }
    
    /// Set up search and results controller
    private func setupNavigationRightBarItems() {
        searchButton.tintColor = .white
        writeOpinionsButton.tintColor = .white
        navigationItem.rightBarButtonItems = [writeOpinionsButton, searchButton]
    }
    
    /// Sets up floating news panel
    private func setUpFloatingPanel() {
        let vc = NewsViewController(viewModel: NewsViewModel(type: .topStories))
        let panel = FloatingPanelController(delegate: viewModel)
        panel.surfaceView.backgroundColor = .secondarySystemBackground
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
        
        let appearance = SurfaceAppearance()
        appearance.backgroundColor = .systemBackground
        appearance.cornerRadius = 10
        panel.surfaceView.appearance = appearance
    }
    
    private func configureMenuBar() {
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        menuBar.delegate = viewModel
        
        view.addSubviews(menuBar)
        
        NSLayoutConstraint.activate([
            menuBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            menuBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            menuBar.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func configureCollectionView() {
        view.addSubviews(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalToSystemSpacingBelow: menuBar.bottomAnchor, multiplier: 2),
            collectionView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 0),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: collectionView.trailingAnchor, multiplier: 0),
            view.safeAreaLayoutGuide.bottomAnchor.constraint(equalToSystemSpacingBelow: collectionView.bottomAnchor, multiplier: 0)
        ])
    }
}
