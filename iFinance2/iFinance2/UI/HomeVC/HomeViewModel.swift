//
//  HomeViewModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import FloatingPanel
import UIKit

protocol HomeViewModelDelegate: AnyObject {
    func myListDidSelect(toPresent viewController: UIViewController)
    func cellSizeForHomeControllerCell() -> CGSize
    func scrollIndicator(to scrollView: UIScrollView)
    func changeNavigationBarButtonStatus(alpha: CGFloat, isEnabled: Bool)
    func scrollToItem(at indexPath: IndexPath, at scrollPosition: UICollectionView.ScrollPosition, animated: Bool)
}

class HomeViewModel: NSObject {
    
    //MARK: - Properties
    private let cells: [UICollectionViewCell] = [MyListCell(),
                                                 OpinionsCell()]
    weak var delegate: HomeViewModelDelegate?
    
    func nice(){
        print(" ho ho")
    }
}

//MARK: - AssetListCell Delegate

extension HomeViewModel: WatchListCellDelegate {
    func myListDidSelect(toPresent viewController: UIViewController) {
        delegate?.myListDidSelect(toPresent: viewController)
    }
}

//MARK: - UICollectionView Data Source

extension HomeViewModel: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        cells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.item {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyListCell.identifier, for: indexPath) as! MyListCell
            cell.delegate = self
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OpinionsCell.identifier, for: indexPath) as! OpinionsCell
            return cell
            
        default:
            break
        }
        
        return UICollectionViewCell()
    }
}

//MARK: - UICollectionViewDelegate Flow Layout

extension HomeViewModel: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let delegate = delegate else {return CGSize(width: 0, height: 0)}
        return delegate.cellSizeForHomeControllerCell()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.scrollIndicator(to: scrollView)
    }
}

//MARK: - FloatingPanelController Delegate

extension HomeViewModel: FloatingPanelControllerDelegate {
    /// Gets floating panel state change
    /// - Parameter fpc: Ref of controller
    func floatingPanelDidChangeState(_ fpc: FloatingPanelController) {
        fpc.state == .full ? delegate?.changeNavigationBarButtonStatus(alpha: 0.25, isEnabled: false) : delegate?.changeNavigationBarButtonStatus(alpha: 1, isEnabled: true)
    }
}

//MARK: - MenuBarView Delegate

extension HomeViewModel: MenuBarViewDelegate {
    func didSelectItemAt(index: Int) {
        let indexPath = IndexPath(item: index, section: 0)
        delegate?.scrollToItem(at: indexPath, at: [], animated: true)
    }
}
