//
//  CellType.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/12/29.
//

import Foundation

protocol CommonDataType { }

struct WatchListData: Decodable, CommonDataType {
    
}

struct CommentListData: Decodable, CommonDataType {
    
}

protocol CellType {
    var identifier: String {get set}
//    func configure(data: CommonDataType)
    
//    var watchListDelegate: WatchListTableViewCellDelegate? { set get }
//    var opinionListDelegate: OpinionsCellDelegate? { set get }
}

extension CellType {
    var watchListDelegate: WatchListTableViewCellDelegate? {
        get {return nil} set {}
    }
    
    var commentListDelegate: OpinionsCellDelegate? {
        get {return nil} set {}
    }
}
