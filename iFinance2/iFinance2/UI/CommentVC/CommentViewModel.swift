//
//  CommentViewModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import FirebaseDatabase
import UIKit

protocol CommentViewModelDelegate: AnyObject {
    func writeCommentButtonDidTap(for symbol: String)
    func updateTableView()
}

class CommentViewModel: NSObject {
    
    weak var delegate: CommentViewModelDelegate?

    //MARK: - Model
    private var postContents: [PostContent] = []

    //MARK: - Properties
    private let database = Database.database().reference().child("specificTalk")
    
    private let symbol: String
    
    //MARK: - Init
    init(symbol: String) {
        self.symbol = symbol
        super.init()
        self.fetchComments()
    }
}

//MARK: - Fetch Comments
extension CommentViewModel {
    private func fetchComments() {
        database.child(symbol).observe(.childAdded) { snapShot in
            guard let dictionary = snapShot.value as? [String: Any] else { return }

            let postContent = PostContent(id: dictionary["id"] as! String,
                                          title: dictionary["title"] as! String,
                                          date: Date(timeIntervalSince1970: dictionary["date"] as! Double),
                                          body: dictionary["body"] as! String)
            
            self.postContents.append(postContent)
            
            DispatchQueue.main.async {
                self.postContents.sort { postContentA, postContentB in
                    postContentA.date > postContentB.date
                }
                
//                self.tableView.reloadData()
                self.delegate?.updateTableView()
            }
        }
    }
}

//MARK: - CommentHeaderView Delegate
extension CommentViewModel: CommentHeaderViewDelegate {
    func commentHeaderViewDidTapAddButton(_ headerView: CommentHeaderView) {
        delegate?.writeCommentButtonDidTap(for: symbol)
    }
}

//MARK: - WriterViewController Delegate
extension CommentViewModel: WriterViewControllerDelegate {
    func saveButtonDidTap(_ postContent: PostContent, _ vc: WriteViewController) {
        ///postContents.append(postContent)
        vc.dismiss(animated: true, completion: nil)
    }
}

//MARK: - TableView Data Source

extension CommentViewModel: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return postContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.identifier, for: indexPath) as? CommentTableViewCell else { return UITableViewCell()}
        cell.configure(with: postContents[indexPath.row])
        return cell
    }
}

//MARK: - TableView Delegate

extension CommentViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        CommentHeaderView.preferredHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView( withIdentifier: CommentHeaderView.identifier) as? CommentHeaderView else { return nil}
        header.configure(with: .init(title: "Comments", shouldShowAddButton: true))
        header.delegate = self
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

