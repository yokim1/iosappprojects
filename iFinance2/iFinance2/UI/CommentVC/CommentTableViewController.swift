//
//  CommentTableViewController.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/08/26.
//
import Firebase
import UIKit


class CommentTableViewController: UITableViewController {
    
    //MARK: - ViewModel
    private let viewModel : CommentViewModel
    
    //MARK: - Init
    init(viewModel: CommentViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.green
        setupTableView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - CommentViewModel Delegate
extension CommentTableViewController: CommentViewModelDelegate {
    func writeCommentButtonDidTap(for symbol: String) {
        let vc = WriteViewController(symbol: symbol)
        present(vc, animated: true, completion: nil)
    }
    
    func updateTableView() {
        tableView.reloadData()
    }
}

//MARK: - Set Up UI
extension CommentTableViewController {
    
    private func setupTableView() {
        tableView.register(CommentHeaderView.self, forHeaderFooterViewReuseIdentifier: CommentHeaderView.identifier)
        tableView.register(CommentTableViewCell.self, forCellReuseIdentifier: CommentTableViewCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.delegate = viewModel
        tableView.dataSource = viewModel
    }
}
