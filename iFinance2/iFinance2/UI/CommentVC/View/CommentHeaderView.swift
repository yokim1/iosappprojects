//
//  CommentHeaderView.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/08/26.
//

import UIKit

/// Delegate to notify of header evnets
protocol CommentHeaderViewDelegate: AnyObject {
    /// Notify user tapped header button
    /// - Parameter headerView: Ref of header view
    func commentHeaderViewDidTapAddButton(_ headerView: CommentHeaderView)
}

/// TableView header for news
final class CommentHeaderView: UITableViewHeaderFooterView {
    /// Header identifier
    static let identifier = "CommentHeaderView"

    /// Ideal height of header
    static let preferredHeight: CGFloat = 60

    /// Delegate instance for evnets
    weak var delegate: CommentHeaderViewDelegate?

    /// ViewModel for header view
    struct ViewModel {
        let title: String
        let shouldShowAddButton: Bool
    }

    // MARK: - Private

    private let label: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 22)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()


    private let writeCommentButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "square.and.pencil"), for: .normal)
        button.tintColor = .white
        button.imageView?.contentMode = .scaleAspectFit
        button.imageView?.bounds = .init(x: 0, y: 0, width: 70, height: 70)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    // MARK: - Init

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        addSubviews(label, writeCommentButton)
        writeCommentButton.addTarget(self, action: #selector(writeCommentButtonDidTap), for: .touchUpInside)
//        label.backgroundColor = .red
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            label.centerYAnchor.constraint(equalTo: centerYAnchor),
            label.heightAnchor.constraint(equalToConstant: 40),
            
            writeCommentButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            writeCommentButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            writeCommentButton.heightAnchor.constraint(equalToConstant: 40),
            writeCommentButton.widthAnchor.constraint(equalToConstant: 40),
        ])
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        label.text = nil
    }

    /// Handle button tap
    @objc private func writeCommentButtonDidTap() {
        // Call delegate
        delegate?.commentHeaderViewDidTapAddButton(self)
    }

    /// Configure view
    /// - Parameter viewModel: View ViewModel
    public func configure(with viewModel: ViewModel) {
        label.text = viewModel.title
        writeCommentButton.isHidden = !viewModel.shouldShowAddButton
    }
}

