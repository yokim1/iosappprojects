//
//  NewsTypeEnum.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import Foundation

/// Type of news
enum NewsType {
    case topStories
    case compan(symbol: String)
    
    /// Title for given type
    var title: String {
        switch self {
        case .topStories:
            return "News"
        case .compan(let symbol):
            return symbol.uppercased()
        }
    }
}
