//
//  TopStoriesNewsViewController.swift
//  Stocks
//
//  Created by 김윤석 on 2021/08/25.
//

import SafariServices
import UIKit

/// Controller to show news
final class NewsViewController: UIViewController {
    
    // MARK: - Properties

    /// Primary news view
    let tableView: UITableView = {
        let table = UITableView()
        // Rgister cell, header
        table.register(NewsStoryTableViewCell.self,
                       forCellReuseIdentifier: NewsStoryTableViewCell.identfier)
        table.register(NewsHeaderView.self,
                       forHeaderFooterViewReuseIdentifier: NewsHeaderView.identifier)
        table.backgroundColor = .clear
        return table
    }()
    
    //MARK: - ViewModel
    
    private let viewModel: NewsViewModel

    // MARK: - Init
    
    init(viewModel: NewsViewModel){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        viewModel.delegate = self
    }

    // MARK: - Private

    /// Sets up tableView
    private func setUpTable() {
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.delegate = viewModel
        tableView.dataSource = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}

extension NewsViewController: NewsViewModelDelegate {
    func newsDidSelectToOpen(_ url: URL) {
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
    
    func newsDidSelectFailedToOpen(_ alertControl: UIAlertController) {
        present(alertControl, animated: true, completion: nil)
    }
    
    func tableViewUpdated() {
        tableView.reloadData()
    }
}
