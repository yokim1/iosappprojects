//
//  NewsViewModel.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/10/03.
//

import UIKit

protocol NewsViewModelDelegate: AnyObject {
    func newsDidSelectToOpen(_ url: URL)
    func newsDidSelectFailedToOpen(_ alertControl: UIAlertController)
    func tableViewUpdated()
}

class NewsViewModel: NSObject {
    
    //MARK: - Model
    private var stories = [NewsStory]()
    
    //MARK: - Delegate
    weak var delegate: NewsViewModelDelegate?
    
    /// Instance of a type
    private let type: NewsType

    init(type: NewsType) {
        self.type = type
        super.init()
        fetchNews()
    }
}

//MARK: -

extension NewsViewModel {
    /// Fetch news models
    private func fetchNews() {
        APICaller.shared.news(for: type) { [weak self] result in
            guard let self = self else {return }
            switch result {
            case .success(let stories):
                DispatchQueue.main.async {
                    self.stories = stories
                    self.delegate?.tableViewUpdated()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    /// Present an alert to show an error occurred when opening story
    private func presentFailedToOpenAlert() {
        HapticsManager.shared.vibrate(for: .error)

        let alert = UIAlertController(
            title: "Unable to Open",
            message: "We were unable to open the article.",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        delegate?.newsDidSelectFailedToOpen(alert)
    }
}

//MARK: - TableView Data Source

extension NewsViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: NewsStoryTableViewCell.identfier,
            for: indexPath
        ) as? NewsStoryTableViewCell else {
            fatalError()
        }
        cell.configure(with: .init(model: stories[indexPath.row]))
        return cell
    }
}

//MARK: - UITableView Delegate

extension NewsViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(
            withIdentifier: NewsHeaderView.identifier
        ) as? NewsHeaderView else { return nil }
        header.configure(with: .init( title: self.type.title, shouldShowAddButton: false))
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return NewsStoryTableViewCell.preferredHeight
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return NewsHeaderView.preferredHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        HapticsManager.shared.vibrateForSelection()

        // Open news story
        let story = stories[indexPath.row]
        guard let url = URL(string: story.url) else {
            presentFailedToOpenAlert()
            return
        }
        delegate?.newsDidSelectToOpen(url)
    }
}
