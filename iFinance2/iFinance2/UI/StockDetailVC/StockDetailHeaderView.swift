//
//  ViewController.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/08/25.
//

import UIKit

/// Header for stock details
final class StockDetailHeaderView: UIView {

    //MARK: - UI Objects
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15, weight: .heavy)
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let priceChangeLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .right
        label.textColor = .white
        label.font = .systemFont(ofSize: 15, weight: .bold)
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 6
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    /// ChartView
    private let chartView = StockChartView()

    /// CollectionView
    private let metricCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(MetricCollectionViewCell.self, forCellWithReuseIdentifier: MetricCollectionViewCell.identifier)
        collectionView.backgroundColor = .secondarySystemBackground
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    private let segmentController: UISegmentedControl = {
       let sc = UISegmentedControl(items: ["1D", "7D", "15D", "1M", "1Y"])
        sc.selectedSegmentIndex = 0
        sc.translatesAutoresizingMaskIntoConstraints = false
        return sc

    }()
    
    /// Metrics viewModels
    private var metricViewModels: [MetricCollectionViewCell.ViewModel] = []

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        clipsToBounds = true
//        addSubviews(priceLabel, priceChangeLabel, chartView, segmentController, metricCollectionView)
        chartView.translatesAutoresizingMaskIntoConstraints = false
        metricCollectionView.delegate = self
        metricCollectionView.dataSource = self
        configureUI()
//        print("Init")
//        print(height)
    }
    
    private func configureUI() {
        let labelStackView = UIStackView(arrangedSubviews: [priceLabel, priceChangeLabel])
        labelStackView.axis = .horizontal
        labelStackView.spacing = 10
        labelStackView.distribution = .equalSpacing
        labelStackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubviews(labelStackView, chartView, segmentController, metricCollectionView)
        
        NSLayoutConstraint.activate([
            labelStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            labelStackView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            labelStackView.heightAnchor.constraint(equalToConstant: 20),
            
            chartView.topAnchor.constraint(equalTo: labelStackView.bottomAnchor, constant: 10),
            chartView.leadingAnchor.constraint(equalTo: leadingAnchor),
            chartView.trailingAnchor.constraint(equalTo: trailingAnchor),
            chartView.heightAnchor.constraint(equalToConstant: 250),
            
            segmentController.topAnchor.constraint(equalTo: chartView.bottomAnchor),
            segmentController.leadingAnchor.constraint(equalTo: leadingAnchor),
            segmentController.trailingAnchor.constraint(equalTo: trailingAnchor),
            segmentController.heightAnchor.constraint(equalToConstant: 30),
            
            metricCollectionView.topAnchor.constraint(equalTo: segmentController.bottomAnchor, constant: 10),
            metricCollectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            metricCollectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            metricCollectionView.heightAnchor.constraint(equalToConstant: 100),
            metricCollectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
        ])
    }

    /// Configure view
    /// - Parameters:
    ///   - chartViewModel: Chart view Model
    ///   - metricViewModels: Collection of metric viewModels
    func configure(quote: Quote?, chartViewModel: StockChartModel,//StockChartView.ViewModel,
                   metricViewModels: [MetricCollectionViewCell.ViewModel]) {
        
        if let quote = quote {
            priceLabel.text = .formatted(number: quote.currentPrice)
            priceChangeLabel.text = .percentage(from: quote.percentChange)
            
            if quote.percentChange < 0 {
                priceChangeLabel.textColor = .systemRed
            } else {
                priceChangeLabel.textColor = .systemGreen
            }
        }
        
        chartView.configure(with: chartViewModel)
        self.metricViewModels = metricViewModels
        metricCollectionView.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
//MARK: - CollectionView Data Source

extension StockDetailHeaderView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return metricViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = metricViewModels[indexPath.row]
        guard let cell = collectionView.dequeueReusableCell( withReuseIdentifier: MetricCollectionViewCell.identifier, for: indexPath) as? MetricCollectionViewCell else { fatalError() }
        cell.configure(with: viewModel)
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout

extension StockDetailHeaderView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: width/2, height: 100/3)
        return CGSize(width: frame.width / 2, height: 100/3)
    }
}
