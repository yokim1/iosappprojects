//
//  StockDetailsViewController.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/08/25.
//

import FloatingPanel
import SafariServices
import UIKit

/// VC to show stock details
final class StockDetailsViewController: UIViewController, FloatingPanelControllerDelegate {
    // MARK: - Properties

    private var quote: Quote?
    
    /// Stock symbol
    private let symbol: String

    /// Company name
    private let companyName: String

    /// Collection of data
    private var candleStickData: [CandleStick]
    
    /// Primary view
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(NewsHeaderView.self,
                       forHeaderFooterViewReuseIdentifier: NewsHeaderView.identifier)
        tableView.register(NewsStoryTableViewCell.self,
                       forCellReuseIdentifier: NewsStoryTableViewCell.identfier)
        
        return tableView
    }()

    /// Collection of news stories
    private var stories: [NewsStory] = []

    /// Company metrics
    private var metrics: Metrics?

    // MARK: - Init

    init(symbol: String, companyName: String, candleStickData: [CandleStick] = []) {
        self.quote = nil
        self.symbol = symbol
        self.companyName = companyName
        self.candleStickData = candleStickData
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = companyName
        
        //navigationController?.navigationBar.isHidden = false
        //navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.topItem?.title = ""
        
//        UIView.animate(withDuration: 1.5) {
//            self.tabBarController?.tabBar.isHidden = true
//        }
        
        //setUpCloseButton()
        setUpTable()
        fetchFinancialData()
        fetchNews()
        
        setUpFloatingPanel()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame             = view.bounds
        tableView.contentInset      = .init(top: 0, left: 0, bottom: view.frame.height/2, right: 0)
    }

    // MARK: - Private

    /// Sets up close button
    private func setUpCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(didTapClose))
    }

    /// Handle close button tap
    @objc private func didTapClose() {
        dismiss(animated: true, completion: nil)
    }

    /// Sets up table
    private func setUpTable() {
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView(
            frame: CGRect(x: 0, y: 0, width: view.width, height: (view.width * 0.7) + 100)
        )
    }

    let numbersOfDays: [TimeInterval] = [1, 7, 15, 30, 360]
    
    /// Fetch financial metrics
    private func fetchFinancialData() {
        let group = DispatchGroup()

        group.enter()
        APICaller.shared.quote(for: symbol) {[weak self] result in
            defer {
                group.leave()
            }
            switch result{
            
            case .success(let response):
                self?.quote = response
            case .failure(let error):
                print(error)
            }
        }
        
        // Fetch candle sticks if needed
        if candleStickData.isEmpty {
            //numbersOfDays.forEach { numberOfDays in
                group.enter()
                APICaller.shared.marketData(for: symbol) { [weak self] result in
                    defer {
                        group.leave()
                    }

                    switch result {
                    case .success(let response):
                        self?.candleStickData = response.candleSticks
//                        self?.candleStickDatas.append(response.candleSticks)
                    case .failure(let error):
                        print(error)
                    }
                }
            }
           
      //  }

        // Fetch financial metrics
        group.enter()
        APICaller.shared.financialMetrics(for: symbol) { [weak self] result in
            defer {
                group.leave()
            }

            switch result {
            case .success(let response):
                let metrics = response.metric
                self?.metrics = metrics
            case .failure(let error):
                print(error)
            }
        }

        group.notify(queue: .main) { [weak self] in
            self?.renderChart()
        }
    }

    /// Fetch news for given type
    private func fetchNews() {
        APICaller.shared.news(for: .compan(symbol: symbol)) { [weak self] result in
            switch result {
            case .success(let stories):
                DispatchQueue.main.async {
                    self?.stories = stories
                    self?.tableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    /// Render chart and metrics
    private func renderChart() {
        // Chart VM | FinancialMetricViewModel(s)
//        let headerView = StockDetailHeaderView( frame: CGRect( x: 0, y: 0, width: view.width, height: (view.width * 0.7) + 100))
        
//        let headerView = StockDetailHeaderView(frame: CGRect( x: 0, y: 0, width: view.width, height: (view.height / 2)))
        let headerView = StockDetailHeaderView()

        var viewModels = [MetricCollectionViewCell.ViewModel]()
        
        if let metrics = metrics {
            viewModels.append(.init(name: "52W High", value: "\(metrics.AnnualWeekHigh)"))
            viewModels.append(.init(name: "52L High", value: "\(metrics.AnnualWeekLow)"))
            viewModels.append(.init(name: "52W Return", value: "\(metrics.AnnualWeekPriceReturnDaily)"))
            viewModels.append(.init(name: "Beta", value: "\(metrics.beta)"))
            viewModels.append(.init(name: "10D Vol.", value: "\(metrics.TenDayAverageTradingVolume)"))
        }

        // Configure
        headerView.configure(quote:
                            quote,
                             chartViewModel:
                                .init(
                                    data: candleStickData.reversed().map { $0.close },
                                    showLegend: true,
                                    showAxis: true,
                                    fillColor: (quote?.percentChange ?? 0) < 0 ? .systemRed : .systemGreen, isFillColor: true),
                                    metricViewModels: viewModels)
        
//        tableView.tableHeaderView = headerView
        
        tableView.tableHeaderView = headerView
        
        //I used dynamic height to make the views fit with autolayout
        if let headerView = tableView.tableHeaderView {

                let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
                var headerFrame = headerView.frame
                
                //Comparison necessary to avoid infinite loop
                //if height != headerFrame.size.height {
                    headerFrame.size.height = height
                    headerView.frame = headerFrame
                    tableView.tableHeaderView = headerView
                //}
        }
    }
    
    private func setUpFloatingPanel() {
        let vc = CommentTableViewController(viewModel: CommentViewModel(symbol: symbol))
        
        let panel = FloatingPanelController(delegate: self)
        panel.surfaceView.backgroundColor = .secondarySystemBackground
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
        
        let appearance = SurfaceAppearance()
//        appearance.backgroundColor = .gray
        appearance.cornerRadius = 10
        panel.surfaceView.appearance = appearance
    }
}

// MARK: - TableView

extension StockDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NewsStoryTableViewCell.identfier,
                                                       for: indexPath) as? NewsStoryTableViewCell else { fatalError()}
        cell.configure(with: .init(model: stories[indexPath.row]))
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return NewsStoryTableViewCell.preferredHeight
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView( withIdentifier: NewsHeaderView.identifier) as? NewsHeaderView else { return nil }
        header.delegate = self
        header.configure( with: .init(title: symbol.uppercased(), shouldShowAddButton: !PersistenceManager.shared.watchlistContains(symbol: symbol)))
        return header
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return NewsHeaderView.preferredHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let url = URL(string: stories[indexPath.row].url) else { return }

        HapticsManager.shared.vibrateForSelection()

        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
}

// MARK: - NewsHeaderViewDelegate

extension StockDetailsViewController: NewsHeaderViewDelegate {
    func newsHeaderViewDidTapAddButton(_ headerView: NewsHeaderView) {

        HapticsManager.shared.vibrate(for: .success)

//        headerView.addToWatchListButton.isHidden = true
        PersistenceManager.shared.addToWatchlist( symbol: symbol, companyName: companyName)

        let alert = UIAlertController(
            title: "Added to Watchlist",
            message: "\(companyName) is successfully added to your watchlist.",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
}
