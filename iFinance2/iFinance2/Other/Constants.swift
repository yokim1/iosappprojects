//
//  Constants.swift
//  iFinance2
//
//  Created by 김윤석 on 2021/09/30.
//

import UIKit

struct ConstantsColor {
    static let themeColor = UIColor(red: 35/255, green: 44/255, blue: 112/255, alpha: 1)
}
