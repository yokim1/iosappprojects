//
//  DatabaseManager.swift
//  KakaoTalkClone
//
//  Created by 김윤석 on 2021/05/25.
//

import Foundation
import FirebaseDatabase

final class DatabaseManager{
    static let shared = DatabaseManager()
    
    private let databse = Database.database().reference()
}

//MARK: - Account Management

extension DatabaseManager {
    func validateNewUser(with email: String) -> Bool {
        
    }
    
    func insertUser(with user: ChatAppUser){
        databse.child(user.email).setValue([
            "name": user.name
        ])
    }
}

struct ChatAppUser{
    let name: String
    let email: String
//    let profilePictureUrl: String
}
