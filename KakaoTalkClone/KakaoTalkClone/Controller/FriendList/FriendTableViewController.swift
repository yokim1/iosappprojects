//
//  FriendTableViewController.swift
//  KakaoTalkClone
//
//  Created by 김윤석 on 2021/05/21.
//

import UIKit
import FirebaseAuth
class FriendTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let isLoggedIn = UserDefaults.standard.bool(forKey: "LoggedIn")
        
        DatabaseManager.shared.test()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        validateAuth()
    }
    
    func validateAuth() {
        if FirebaseAuth.Auth.auth().currentUser == nil {
            
            let loginViewController = storyboard?.instantiateViewController(identifier: "LoginViewController")
            
            loginViewController?.modalPresentationStyle = .fullScreen
            present(loginViewController ?? LoginViewController(), animated: true, completion: nil)
        }
        
        print(FirebaseAuth.Auth.auth().currentUser)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 10
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FriendListCell", for: indexPath) as? FriendListCell else {return UITableViewCell()}
        
        cell.profileName.text = "김갑생"
        cell.profileImage.layer.cornerRadius = 15.0;
        cell.profileImage.layer.masksToBounds = true;
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let profileViewController = storyboard?.instantiateViewController(identifier: "ProfileViewController") else {return }
        
        present(profileViewController, animated: true, completion: nil)
    }
    
    @IBAction func addFriendButtonDidTapped(_ sender: Any) {
    }
    
}


class FriendListCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var statusMessage: UILabel!
    
}
