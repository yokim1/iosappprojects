//
//  RegisterViewController.swift
//  KakaoTalkClone
//
//  Created by 김윤석 on 2021/05/21.
//

import UIKit
import FirebaseAuth

class RegisterViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var profilePictureUIImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .blue
        profilePictureUIImageView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapChangeProfilePicture))
        
        profilePictureUIImageView.addGestureRecognizer(gesture)
    }
    
    @objc private func didTapChangeProfilePicture(){
        showPhotoActionSheet()
    }
    
    @IBAction func registerButtonDidTapped(_ sender: Any) {
        guard let email = emailTextField.text,
              let password = passwordTextField.text,
              let name = nameTextField.text else {return}
        
        FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password) { [weak self] dataResult, error in
            
            DatabaseManager.shared.insertUser(with: ChatAppUser(name: name,
                                                                email: email))
            
            self?.dismiss(animated: true, completion: nil)
        }
    }

}

extension RegisterViewController: UIImagePickerControllerDelegate {
    func showPhotoActionSheet(){
        let actionSheet = UIAlertController(title: "Profile Picture", message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Take photo", style: .default, handler: { [weak self]_ in
            self?.presentCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Choose photo", style: .default, handler: { [weak self]_ in
            self?.presentPhotoPicker()
        }))
        
        present(actionSheet, animated: true)
    }
    
    func presentCamera(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    func presentPhotoPicker() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        profilePictureUIImageView.image = selectedImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
