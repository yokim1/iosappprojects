//
//  LoginViewController.swift
//  KakaoTalkClone
//
//  Created by 김윤석 on 2021/05/21.
//

import UIKit
import FirebaseAuth
class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.view.backgroundColor = .red
        
    }

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func logInButtonDidTapped(_ sender: Any) {
        guard let email = emailTextField.text,
              let password = passwordTextField.text,
              !email.isEmpty, !password.isEmpty else {
            alertLoginError()
            return }
        
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password) { [weak self] dataResult, error in
            if let error = error {
                print(error)
                return
            }
            
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    func alertLoginError() {
        
    }
    
    @IBAction func registerButtonDidTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "RegisterViewController")
        present(vc ?? RegisterViewController(), animated: true, completion: nil)
    }
}
