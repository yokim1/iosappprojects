//
//  UIColor+Ext.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/11/30.
//

import UIKit

extension UIColor {
    static let themeColor = UIColor(red: 223/255, green: 86/255, blue: 111/255, alpha: 1.0)
    static let timerColor = UIColor(red: 234/255, green: 120/255, blue: 47/255, alpha: 1.0)
    static let pulseColor = UIColor(red: 86/255, green: 30/255, blue: 63/255, alpha: 1.0)
    static let bazelBackgroundColor = UIColor(red: 56/255, green: 25/255, blue: 49/255, alpha: 1)
}
