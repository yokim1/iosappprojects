//
//  Constants.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//

import UIKit

struct Constants {
    static let buttonSize: CGFloat = 50
    
    static let leadingPadding: CGFloat = 16
    static let trailingPadding: CGFloat = -16
}

struct PaddingConstants {
    static let leadingPadding: CGFloat = 16
    static let trailingPadding: CGFloat = -16
}
