//
//  BasicViewController.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/11/29.
//

import UIKit

class AdrenalistTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().tintColor = .themeColor
        
        UINavigationBar.appearance().tintColor = .themeColor
        
        viewControllers = [
            navigationControllerCreator(viewController: MainViewController(),
                                        tabbarImage: UIImage(systemName: "circle"),
                                        selectedTabbarImage: UIImage(systemName: "smallcircle.circle")),
            
            navigationControllerCreator(viewController: CalanderViewController(),
                                        tabbarImage: UIImage(systemName: "calendar"),
                                        selectedTabbarImage: UIImage(systemName: "calendar.fill"))
        ]
    }
    
    func navigationControllerCreator(viewController: UIViewController, tabbarImage: UIImage?, selectedTabbarImage: UIImage?) -> UINavigationController {
        viewController.tabBarItem = UITabBarItem(title: title, image: tabbarImage, selectedImage: selectedTabbarImage)
        return UINavigationController(rootViewController: viewController)
    }
}
