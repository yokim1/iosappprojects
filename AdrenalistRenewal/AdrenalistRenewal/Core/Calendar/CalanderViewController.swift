//
//  CalanderViewController.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//
import FSCalendar
import UIKit

final class CalendarHeaderView: UIView, FSCalendarDataSource, FSCalendarDelegate {
    private let calendar = FSCalendar()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        calendar.dataSource = self
        calendar.delegate = self
        addSubviews(calendar)
        
        NSLayoutConstraint.activate([
            calendar.leadingAnchor.constraint(equalTo: leadingAnchor),
            calendar.trailingAnchor.constraint(equalTo: trailingAnchor),
            calendar.topAnchor.constraint(equalTo: topAnchor),
            calendar.heightAnchor.constraint(equalToConstant: 350)
        ])
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("tapped")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class CalanderViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        tableView.tableHeaderView = CalendarHeaderView()
        view.backgroundColor = .white
        
        let uv = CalendarHeaderView()
        tableView.tableHeaderView = uv
        
        //I used dynamic height to make the views fit with autolayout
        if let headerView = tableView.tableHeaderView {
            
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            headerFrame.size.height = height
            headerView.frame = headerFrame
            tableView.tableHeaderView = headerView
        }
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = .red
        return cell
    }
}
