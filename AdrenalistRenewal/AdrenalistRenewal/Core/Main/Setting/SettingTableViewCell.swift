//
//  File.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/04.
//

import UIKit

final class SettingTableViewCell: UITableViewCell {
    static let identifier = "SettingTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
