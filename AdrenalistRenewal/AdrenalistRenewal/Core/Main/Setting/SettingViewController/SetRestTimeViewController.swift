//
//  SetRestTimeViewController.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/04.
//

import UIKit

class SetRestTimeButton: UIButton {
    
    init(title: String) {
        super.init(frame: .zero)
        self.setTitle(title, for: .normal)
    }
    
    override var isHighlighted: Bool {
        get { return super.isHighlighted }
        set {
            guard newValue != isHighlighted else { return }
            
            if newValue == true {
                titleLabel?.alpha = 0.25
            } else {
                UIView.animate(withDuration: 0.25) {
                    self.titleLabel?.alpha = 1
                }
                super.isHighlighted = newValue
            }
            
            super.isHighlighted = newValue
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ResetTimeButton: UIButton {
    
    init(title: String) {
        super.init(frame: .zero)
        self.setTitle(title, for: .normal)
    }
    
    override var isHighlighted: Bool {
        get { return super.isHighlighted }
        set {
            guard newValue != isHighlighted else { return }
            
            if newValue == true {
                titleLabel?.alpha = 0.25
            } else {
                UIView.animate(withDuration: 0.25) {
                    self.titleLabel?.alpha = 1
                }
                super.isHighlighted = newValue
            }
            
            super.isHighlighted = newValue
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class SetRestTimeViewController: UIViewController, SettingViewControllerType {
    var name: String = "Set Rest Time"
    
    private let restTimeLabel = UILabel()
    
    private let fiveSecRestButton = SetRestTimeButton(title: "+ 5 Sec")
    private let tenSecRestButton = SetRestTimeButton(title: "+ 10 Sec")
    private let thirtySecRestButton = SetRestTimeButton(title: "+ 30 Sec")
    
    private let resetButton = ResetTimeButton(title: "Reset Timer")
    
    private var timer: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateRestTimeLabel()
        configureButtons()
        configureButtonActions()
        
        let completeButton = UIBarButtonItem(title: "Complete", style: .done, target: self, action: #selector(completeButtonDidTap))
        navigationController?.navigationItem.rightBarButtonItems = [completeButton]
    }
    
    @objc
    private func completeButtonDidTap() {
        // put timer value in TimerManager
    }
    
    private func configureButtonActions() {
        fiveSecRestButton.addTarget(self, action: #selector(fiveSecRestButtonDidTap), for: .touchUpInside)
        tenSecRestButton.addTarget(self, action: #selector(tenSecRestButtonDidTap), for: .touchUpInside)
        thirtySecRestButton.addTarget(self, action: #selector(thirtySecRestButtonDidTap), for: .touchUpInside)
        resetButton.addTarget(self, action: #selector(resetButtonDidTap), for: .touchUpInside)
    }
    
    @objc
    private func fiveSecRestButtonDidTap() {
        TimerManager.shared.timeToBeSaved.countUptoThisSec += 5
        updateRestTimeLabel()
    }
    
    @objc
    private func tenSecRestButtonDidTap() {
        TimerManager.shared.timeToBeSaved.countUptoThisSec += 10
        updateRestTimeLabel()
    }
    
    @objc
    private func thirtySecRestButtonDidTap() {
        TimerManager.shared.timeToBeSaved.countUptoThisSec += 30
        updateRestTimeLabel()
    }
    
    @objc
    private func resetButtonDidTap() {
        TimerManager.shared.timeToBeSaved.countUptoThisSec = 0
        updateRestTimeLabel()
    }
    
    private func updateRestTimeLabel(){
        restTimeLabel.text = "\(TimerManager.shared.timeToBeSaved.countUptoThisSec) Sec"
    }
}

//MARK: - configure UI
extension SetRestTimeViewController {
   
    
    private func configureButtons() {
        let restTimeButtonStackView = UIStackView(arrangedSubviews: [fiveSecRestButton, tenSecRestButton, thirtySecRestButton])
        restTimeButtonStackView.axis = .vertical
        restTimeButtonStackView.spacing = 12
        
        view.addSubviews(restTimeLabel, restTimeButtonStackView, resetButton)
        
        NSLayoutConstraint.activate([
            restTimeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            restTimeLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            
            restTimeButtonStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            restTimeButtonStackView.topAnchor.constraint(equalTo: restTimeLabel.bottomAnchor, constant: 30),
            
            resetButton.topAnchor.constraint(equalTo: restTimeButtonStackView.bottomAnchor, constant: 30),
            resetButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}
