//
//  SettingView.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/04.
//

import UIKit

protocol SettingViewControllerType {
    var name: String {get set}
}
