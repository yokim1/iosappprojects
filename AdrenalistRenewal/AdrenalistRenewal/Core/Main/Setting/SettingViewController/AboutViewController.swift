//
//  AboutViewController.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/04.
//

import UIKit

final class AboutViewController: UIViewController, SettingViewControllerType {
    var name: String = "About"
    
    private let descriptionLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

        descriptionLabel.text = "Developed by Yoon Kim @ 2021"
        
        view.addSubviews(descriptionLabel)
        
        NSLayoutConstraint.activate([
            descriptionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            descriptionLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}
