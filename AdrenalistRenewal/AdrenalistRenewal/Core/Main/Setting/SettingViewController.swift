//
//  SettingViewController.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//

import UIKit

protocol SettingViewControllerDelegate: AnyObject {
    func settingViewControllerCloseButtonDidTap()
}

final class SettingTableViewController: UITableViewController {
    
    weak var delegate: SettingViewControllerDelegate?
    
    private let settingViewControllers: [SettingViewControllerType] = [
        SetRestTimeViewController(),
        AboutViewController()
    ]
    
    init() {
        super.init(style: .insetGrouped)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureRightBarButtons()
        tableView.register(SettingTableViewCell.self, forCellReuseIdentifier: SettingTableViewCell.identifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK: - Action Buttons
extension SettingTableViewController {
    @objc
    private func closeButtonDidTap() {
        delegate?.settingViewControllerCloseButtonDidTap()
    }
}

//MARK: - TableView DataSource
extension SettingTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        settingViewControllers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingTableViewCell.identifier, for: indexPath) as? SettingTableViewCell else {return UITableViewCell()}
        cell.textLabel?.text = settingViewControllers[indexPath.row].name
        return cell
    }
}

//MARK: - TableView Delegate
extension SettingTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let vc = settingViewControllers[indexPath.item] as? UIViewController else {return }
        navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - configure UI
extension SettingTableViewController {
    private func configureRightBarButtons() {
        let closeButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .done, target: self, action: #selector(closeButtonDidTap))
        navigationItem.rightBarButtonItems = [closeButton]
    }
}
