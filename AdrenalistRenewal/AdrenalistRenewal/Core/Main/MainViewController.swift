//
//  ViewController.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//

import UIKit

final class MainViewController: UIViewController {
    
    //MARK: - UI Objects
    private lazy var bazel              = BazelCounter(BazelCounterDefaultAnimator(), of: view, numberOfTaps: 2)
    private lazy var currentWorkoutLabel    = AdrenalistTitleLabel()
    private lazy var nextWorkoutLabel       = AdrenalistTitleLabel()
    
    //MARK: - Persistance Manager
    private let persistanceManager          = PersistanceManager()
    
    //MARK: - Model
    private var workout: WorkOutsResponse?
    
    //MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
        
        fetchWorkout()
        
        TimerManager.shared.timeToBeSaved.countUptoThisSec = 10
        
        configureLeftBarButtons()
        configureRightBarButtons()
        
        configureBazel()
        configureLabels()
        
        updateBazelAnimation()
        updateWorkoutLabels()
    }
    
    //MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Bazel Tap Action
extension MainViewController: BazelCounterDelegate {
    func bazelDidTap(_ countableObjects: [BazelCountable], objectsAt index: Int) {
        guard let workouts = countableObjects as? [WorkOut] else { return }
        persistanceManager.saveCurrentWorkout(WorkOutsResponse(currentIndex: index
                                                               , workouts: workouts))
        updateWorkoutLabels()
        workout?.currentIndex = index
    }
}

//MARK: - SettingViewController Delegate
extension MainViewController: SettingViewControllerDelegate {
    func settingViewControllerCloseButtonDidTap() {
        dismiss(animated: true) {
            self.updateBazelAnimation()
        }
    }
}

//MARK: - WorkoutListTableViewController Delegate
extension MainViewController: WorkoutListTableViewControllerDelegate {
    func WorkoutListTableViewControllerCloseButtonDidTap(_ workouts: [WorkOut]) {
        dismiss(animated: true) {
            self.bazel.primaryCountableObjects = workouts
            self.updateBazelAnimation()
        }
    }
}

//MARK: - fetchWorkout Data
private extension MainViewController {
    private func fetchWorkout() {
        guard let fetchedWorkout = persistanceManager.fetchWorkout() else {return }
        workout = fetchedWorkout
    }
}

//MARK: - Button Actions
/// SettingButton
/// Workout List Button
private extension MainViewController {
    @objc
    func settingButtonDidTap() {
        let vc = SettingTableViewController()
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
    
    @objc
    func workoutListButtonDidTap() {
        let vc = WorkoutListTableViewController()
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
}

//MARK: - Update UI
private extension MainViewController {
    func updateBazelAnimation() {
        bazel.reloadPulsingAnimation()
        bazel.reloadOutlineStrokeAnimation()
    }
    
    func updateWorkoutLabels() {
        guard let index = workout?.currentIndex else {return }
        print(index)
        currentWorkoutLabel.text = workout?.workouts[index].name
    }
}

//MARK: - Configure UI
private extension MainViewController {
    func configureBazel() {
        bazel.delegate = self
        bazel.colorSet(pulsingLayer: .pulseColor,
                       outlineStrokeLayer: .themeColor,
                       trackLayer: .bazelBackgroundColor,
                       timerInsideStrokeLayer: .timerColor)
        bazel.primaryCountableObjects = workout?.workouts ?? []
        bazel.drawLayers()
    }
    
    func configureLabels() {
        nextWorkoutLabel.textColor = .lightText
        
        view.addSubviews(currentWorkoutLabel, nextWorkoutLabel)
        NSLayoutConstraint.activate([
            currentWorkoutLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            currentWorkoutLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            nextWorkoutLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            nextWorkoutLabel.topAnchor.constraint(equalTo: currentWorkoutLabel.bottomAnchor, constant: 24)
        ])
    }
    
    func configureLeftBarButtons() {
        let settingButton = UIBarButtonItem(image: UIImage(systemName: "gear"), style: .done, target: self, action: #selector(settingButtonDidTap))
        navigationItem.leftBarButtonItems = [settingButton]
    }
    
    func configureRightBarButtons() {
        let workoutListButton = UIBarButtonItem(image: UIImage(systemName: "list.dash"), style: .done, target: self, action: #selector(workoutListButtonDidTap))
        navigationItem.rightBarButtonItems = [workoutListButton]
    }
}
