//
//  WorkoutListTableViewController.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//

import UIKit

protocol WorkoutListTableViewControllerDelegate: AnyObject {
    func WorkoutListTableViewControllerCloseButtonDidTap(_ workouts: [WorkOut])
}

final class WorkoutListTableViewController: UITableViewController {
    
    private var workouts: [WorkOut] = []
    
    weak var delegate: WorkoutListTableViewControllerDelegate?
    
    private let persistanceManager = PersistanceManager()
    
    init() {
        super.init(style: .insetGrouped)
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //configure workouts with Persistance Manager
        configureLeftBarButtons()
        configureRightBarButtons()
        configureTableView()
        
        workouts = self.persistanceManager.fetchWorkout()?.workouts ?? []
        tableView.reloadData()
        
//        tableView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(cellDidPressed)))
//        tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellDidTapped)))
    }
    
    @objc func cellDidPressed(sender: UIView) {
        tableView.isEditing = true
    }
    
    @objc func cellDidTapped(sender: UIView) {
        tableView.isEditing = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Button Actions
extension WorkoutListTableViewController {
    @objc
    private func doneForTodayButtonDidTap() {
        
    }
    
    @objc
    private func addWorkoutButtonDidTap() {
        workouts.append(.init(name: "Workout", reps: 20, weight: 20, isDone: false))
        let newIndexPath = IndexPath(row: workouts.count - 1, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .top)
    }
    
    @objc
    private func closeButtonDidTap() {
        delegate?.WorkoutListTableViewControllerCloseButtonDidTap(workouts)
    }
}

//MARK: - Data Source
extension WorkoutListTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        workouts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WorkoutListTableViewCell.identifier, for: indexPath) as? WorkoutListTableViewCell else { return UITableViewCell()}
        cell.configure(with: workouts[indexPath.item])
        return cell
    }
}

//MARK: - Delegate
extension WorkoutListTableViewController {
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        workouts.remove(at: sourceIndexPath.row)
        let moveCell = workouts[sourceIndexPath.row]
        workouts.insert(moveCell, at: destinationIndexPath.row)
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        workouts.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .left)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        workouts[indexPath.row].isDone = !workouts[indexPath.row].isDone
        let reloadIndexPath = IndexPath(row: indexPath.row, section: 0)
        tableView.reloadRows(at: [reloadIndexPath], with: .fade)
        
        persistanceManager.saveCurrentWorkout(WorkOutsResponse(currentIndex: indexPath.row, workouts: workouts))
    }
}

//MARK: - Configure UI
extension WorkoutListTableViewController {
    private func configureTableView() {
        tableView.register(WorkoutListTableViewCell.self, forCellReuseIdentifier: WorkoutListTableViewCell.identifier)
        tableView.rowHeight = 60
    }
    
    private func configureLeftBarButtons() {
        let doneForTodayButton = UIBarButtonItem(title: "Done for today", style: .done, target: self, action: #selector(doneForTodayButtonDidTap))
        navigationItem.leftBarButtonItems = [doneForTodayButton]
    }
    
    private func configureRightBarButtons() {
        let addWorkoutButton = UIBarButtonItem(image: UIImage(systemName: "plus"), style: .done, target: self, action: #selector(addWorkoutButtonDidTap))
        
        let closeButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .done, target: self, action: #selector(closeButtonDidTap))
        navigationItem.rightBarButtonItems = [closeButton, addWorkoutButton]
    }
}
