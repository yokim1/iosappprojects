//
//  PersistanceManager.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/03.
//

import UIKit

final class PersistanceManager {
    
    private let userDefaults = UserDefaults.standard
    
    func saveCurrentWorkout(_ workouts: WorkOutsResponse) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(workouts) {
            let defaults = userDefaults
            defaults.set(encoded, forKey: Constants.savedWorkoutKey)
        }
    }
    
    func fetchWorkout() -> WorkOutsResponse? {
        let defaults = userDefaults
        if let fetchedWorkouts = defaults.object(forKey: Constants.savedWorkoutKey) as? Data {
            let decoder = JSONDecoder()
            if let myInfo = try? decoder.decode(WorkOutsResponse.self, from: fetchedWorkouts) {
                return myInfo
            }
        }
        return nil
    }
}

extension PersistanceManager {
    struct Constants {
        static let savedWorkoutKey = "SavedWorkoutKey"
    }
}
