//
//  TimerManager.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/04.
//

import UIKit

final class TimerManager {
    static let shared = TimerManager()
    
    var timer = Timer()
    
    var isTimeRunning: Bool = false
    
    var timeToBeSaved = TimeToBeSaved()
    
    var onGoingTime: Int = 0

    private init() { }
    
}

struct TimeToBeSaved: Codable {
    var countUptoThisSec: Int = 0
    var exitDateTime: Date?
}
