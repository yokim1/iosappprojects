//
//  WorkOut.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/05.
//

import UIKit

struct WorkOutsResponse: Codable {
    var currentIndex: Int
    let workouts: [WorkOut]
}

struct WorkOut: Equatable, Codable, BazelCountable {
    let name: String
    let reps: Int
    let weight: Float
    var isDone: Bool
}
