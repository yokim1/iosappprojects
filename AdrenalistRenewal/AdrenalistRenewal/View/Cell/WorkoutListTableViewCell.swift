//
//  WorkoutListTableViewCell.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//

import UIKit

final class WorkoutListTableViewCell: UITableViewCell {
    static let identifier = "WorkoutListTableViewCell"
    static let height: CGFloat = 60
    
    //MARK: - UI Objects
    private let circularImageView = UIImageView()
    private let workoutLabel = UILabel()
    private let repsLabel = UILabel()
    private let weightsLabel = UILabel()
    
    //MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLabels()
    }
    
    public func configure(with workout: WorkOut) {
        
        if workout.isDone {
            circularImageView.image = UIImage(systemName: "circle.fill")
        } else {
            circularImageView.image = UIImage(systemName: "circle")
        }
        workoutLabel.text = workout.name
        repsLabel.text = "\(workout.reps)"
        weightsLabel.text = "\(workout.weight) kg"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Configure UI
extension WorkoutListTableViewCell{
    
    private func configureLabels() {
        contentView.addSubviews(circularImageView, workoutLabel, repsLabel, weightsLabel)
        circularImageView.layer.cornerRadius = 30/2
        
        NSLayoutConstraint.activate([
            circularImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.leadingPadding),
            circularImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            circularImageView.heightAnchor.constraint(equalToConstant: 30),
            circularImageView.widthAnchor.constraint(equalToConstant: 30),
            
            workoutLabel.leadingAnchor.constraint(equalTo: circularImageView.trailingAnchor,constant: Constants.leadingPadding),
            workoutLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            repsLabel.leadingAnchor.constraint(equalTo: contentView.centerXAnchor),
            repsLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            weightsLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: Constants.trailingPadding),
            weightsLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
}
