//
//  File.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//

import UIKit

final class AdrenalistImageButton: UIButton {
    
    convenience init(image: UIImage?) {
        self.init(frame: .zero)
        
        setImage(image, for: .normal)
        tintColor = .red
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
