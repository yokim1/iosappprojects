//
//  File.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/09/22.
//

import UIKit

final class AdrenalistTextButton: UIButton {
    
    convenience init(text: String) {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
