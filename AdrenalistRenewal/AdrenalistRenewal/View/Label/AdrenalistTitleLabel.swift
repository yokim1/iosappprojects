//
//  Label.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/05.
//

import UIKit

class AdrenalistTitleLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        font = .systemFont(ofSize: 24, weight: .semibold)
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
