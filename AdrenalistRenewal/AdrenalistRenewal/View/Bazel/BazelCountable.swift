//
//  BazelCountable.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/05.
//

import UIKit

protocol BazelCountable {
    var isDone: Bool { get set }
}
