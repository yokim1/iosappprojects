//
//  BazelCounterAnimateable.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/05.
//

import UIKit

protocol BazelPrimaryCounterAnimateable {
    var primaryCountableObjects: [BazelCountable] { get set }
    func animatePrimaryStroke(primaryStroke: CAShapeLayer)
}

protocol BazelSecondaryCounterAnimateable {
    var secondaryCountableObjects: [BazelCountable] { get set }
    func animateSecondaryStroke(secondaryStroke: CAShapeLayer)
}

protocol BazelPulsingAnimateable{
    func animatePulsing(pulsingLayer: CAShapeLayer)
}
