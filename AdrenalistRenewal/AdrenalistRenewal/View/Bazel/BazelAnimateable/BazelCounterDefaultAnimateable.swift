//
//  File.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/05.
//

import UIKit
//
//protocol BazelCounterDefaultAnimateable: BazelPulsingAnimateable, BazelPrimaryCounterAnimateable, BazelSecondaryCounterAnimateable { }
//
////MARK: - Pulsing
//extension BazelCounterDefaultAnimateable {
//    func animatePulsing(pulsingLayer: CAShapeLayer) {
//        let animation = CABasicAnimation(keyPath: "transform.scale")
//
//        animation.toValue = (primaryCountableObjects.count == 0) ? 1.0 : 1.5
//        animation.fromValue = 1.25
//        animation.duration = self.progressResult(primaryCountableObjects)
//        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
//        animation.autoreverses = true
//        animation.repeatCount = Float.infinity
//        pulsingLayer.add(animation, forKey: "pulsing")
//    }
//}
//
////MARK: - Primary Stroke
//extension BazelCounterDefaultAnimateable {
//    func animatePrimaryStroke(primaryStroke: CAShapeLayer) {
//        primaryStroke.strokeEnd = self.calculateDonePercentage(primaryCountableObjects) / 100.0
//
//        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
//        basicAnimation.duration = 0.5
//        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
//        primaryStroke.add(basicAnimation, forKey: "Basic")
//    }
//}
//
////MARK: - Secondary Stroke
//extension BazelCounterDefaultAnimateable {
//    func animateSecondaryStroke(secondaryStroke: CAShapeLayer) {
//        if secondaryStroke.strokeEnd == 1.0 || TimerManager.shared.timeToBeSaved.countUptoThisSec + 1 <= TimerManager.shared.onGoingTime {
//
//            secondaryStroke.strokeEnd = 0
//            secondaryStroke.isHidden = true
//
//            TimerManager.shared.timer.invalidate()
//            TimerManager.shared.onGoingTime = 0
//            TimerManager.shared.isTimeRunning = false
//            return
//        }
//
//        secondaryStroke.strokeEnd = CGFloat(TimerManager.shared.onGoingTime) / CGFloat(TimerManager.shared.timeToBeSaved.countUptoThisSec)
//        secondaryStroke.isHidden = false
//        TimerManager.shared.onGoingTime += 1
//
//        //MARK: TIMER DESIGN HERE
//        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
//        basicAnimation.duration = 1
//        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
//        secondaryStroke.add(basicAnimation, forKey: "Basic")
//    }
//}
//
//extension BazelCounterDefaultAnimateable {
//    private func progressResult(_ workouts: [BazelCountable]) -> CFTimeInterval {
//        let finishedWorkOut = CGFloat( workouts.filter { !$0.isDone }.count)
//        let totalWorkout = CGFloat( workouts.count)
//        let timeInterval = CFTimeInterval(finishedWorkOut / totalWorkout)
//        return 0.2 >= timeInterval ? 0.2 : timeInterval
//    }
//
//    private func calculateDonePercentage(_ workouts: [BazelCountable]) -> CGFloat {
//        let numerator = CGFloat(workouts.filter { $0.isDone }.count)
//        let denominator = CGFloat(workouts.count)
//        return (numerator / denominator) * 100.0
//    }
//}

protocol BazelCounterDefaultAnimateable: BazelPulsingAnimateable, BazelPrimaryCounterAnimateable, BazelSecondaryCounterAnimateable { }

class BazelCounterDefaultAnimator: BazelCounterDefaultAnimateable {
    var primaryCountableObjects: [BazelCountable] = []
    var secondaryCountableObjects: [BazelCountable] = []
    
    func animatePulsing(pulsingLayer: CAShapeLayer) {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.toValue = (primaryCountableObjects.count == 0) ? 1.0 : 1.5
        animation.fromValue = 1.25
        animation.duration = self.progressResult(primaryCountableObjects)
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        pulsingLayer.add(animation, forKey: "pulsing")
    }
    
    func animatePrimaryStroke(primaryStroke: CAShapeLayer) {
        primaryStroke.strokeEnd = self.calculateDonePercentage(primaryCountableObjects) / 100.0
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.duration = 0.5
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        primaryStroke.add(basicAnimation, forKey: "Basic")
    }
    
    func animateSecondaryStroke(secondaryStroke: CAShapeLayer) {
        if secondaryStroke.strokeEnd == 1.0 || TimerManager.shared.timeToBeSaved.countUptoThisSec + 1 <= TimerManager.shared.onGoingTime {
            
            secondaryStroke.strokeEnd = 0
            secondaryStroke.isHidden = true
            
            TimerManager.shared.timer.invalidate()
            TimerManager.shared.onGoingTime = 0
            TimerManager.shared.isTimeRunning = false
            return
        }
        
        secondaryStroke.strokeEnd = CGFloat(TimerManager.shared.onGoingTime) / CGFloat(TimerManager.shared.timeToBeSaved.countUptoThisSec)
        secondaryStroke.isHidden = false
        TimerManager.shared.onGoingTime += 1
        
        //MARK: TIMER DESIGN HERE
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.duration = 1
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        secondaryStroke.add(basicAnimation, forKey: "Basic")
    }
    
    private func progressResult(_ workouts: [BazelCountable]) -> CFTimeInterval {
        let finishedWorkOut = CGFloat( workouts.filter { !$0.isDone }.count)
        let totalWorkout = CGFloat( workouts.count)
        let timeInterval = CFTimeInterval(finishedWorkOut / totalWorkout)
        return 0.2 >= timeInterval ? 0.2 : timeInterval
    }
    
    private func calculateDonePercentage(_ workouts: [BazelCountable]) -> CGFloat {
        let numerator = CGFloat(workouts.filter { $0.isDone }.count)
        let denominator = CGFloat(workouts.count)
        return (numerator / denominator) * 100.0
    }
    
}
