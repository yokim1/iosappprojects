//
//  File.swift
//  AdrenalistRenewal
//
//  Created by 김윤석 on 2021/12/02.
//

import UIKit

protocol BazelCounterDelegate: AnyObject {
    func bazelDidTap(_ countableObjects: [BazelCountable], objectsAt index: Int)
}

class BazelCounter {
    
    //MARK: - UI Objects
    private let pulsingLayer            = CAShapeLayer()
    private let primaryStrokeLayer      = CAShapeLayer()
    private let trackLayer              = CAShapeLayer()
    private let secondaryStrokeLayer    = CAShapeLayer()
    private let circularPath = UIBezierPath(arcCenter: .zero, radius: 150, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
    
    weak var delegate: BazelCounterDelegate?
    
    private let view: UIView
    
    private var currentIndex: Int
    
    private var animator: BazelCounterDefaultAnimateable
    
    //MARK: - Model
    var primaryCountableObjects: [BazelCountable] = []
    var secondaryCountableObjects: [BazelCountable] = []
    
    //MARK: - Init
    init(_ animator: BazelCounterDefaultAnimateable, of view: UIView, numberOfTaps: Int = 2) {
        self.animator = animator
        self.view = view
        self.currentIndex = 0
        
        self.animator.primaryCountableObjects    = primaryCountableObjects
        self.animator.secondaryCountableObjects  = secondaryCountableObjects
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(bazelDidTap))
        gesture.numberOfTapsRequired = numberOfTaps
        view.addGestureRecognizer(gesture)
    }
    
    func colorSet(pulsingLayer: UIColor = .pulseColor,
                  outlineStrokeLayer: UIColor,
                  trackLayer: UIColor,
                  timerInsideStrokeLayer: UIColor) {
        
        self.pulsingLayer.fillColor             = pulsingLayer.cgColor
        self.primaryStrokeLayer.strokeColor     =  outlineStrokeLayer.cgColor
        self.trackLayer.strokeColor             = trackLayer.cgColor
        self.secondaryStrokeLayer.strokeColor   = timerInsideStrokeLayer.cgColor
    }
}

//MARK: - Configure Circular Design View
extension BazelCounter {
    public func drawLayers() {
        self.drawPulsingLayer()
        self.drawTrackLayer()
        self.drawPrimaryLayer()
        self.drawSecondaryLayer()
    }
    
    public func reloadOutlineStrokeAnimation() {
//        animatePrimaryStroke(primaryStroke: primaryStrokeLayer)
        animator.animatePrimaryStroke(primaryStroke: primaryStrokeLayer)
    }
    
    public func reloadPulsingAnimation() {
//        animatePulsing(pulsingLayer: pulsingLayer)
        animator.animatePulsing(pulsingLayer: pulsingLayer)
    }
}

//MARK: - Tap Action
private extension BazelCounter {
    @objc
    func bazelDidTap() {
        currentIndex = getCurrentIndex()
        completeCurrentWorkOut()
        delegate?.bazelDidTap(primaryCountableObjects, objectsAt: currentIndex)
        
        animator.animatePrimaryStroke(primaryStroke: primaryStrokeLayer)
        animator.animatePulsing(pulsingLayer: pulsingLayer)
        
        if TimerManager.shared.isTimeRunning == false {
            TimerManager.shared.isTimeRunning = true
            TimerManager.shared.onGoingTime = 1
            
            TimerManager.shared.timer = Timer.scheduledTimer(timeInterval: 1,
                                                             target: self,
                                                             selector: #selector(self.animateTimerAsTimeFlows),
                                                             userInfo: nil,
                                                             repeats: true)
        }
        else if TimerManager.shared.isTimeRunning == true {
            TimerManager.shared.isTimeRunning = false
            TimerManager.shared.timer.invalidate()
            TimerManager.shared.onGoingTime = 0
            animator.animateSecondaryStroke(secondaryStroke: secondaryStrokeLayer)
        }
    }
    
    @objc private func animateTimerAsTimeFlows() {
//        animateSecondaryStroke(secondaryStroke: secondaryStrokeLayer)
        animator.animateSecondaryStroke(secondaryStroke: secondaryStrokeLayer)
    }
    
    func getCurrentIndex() -> Int {
        for index in 0..<primaryCountableObjects.count {
            if primaryCountableObjects[index].isDone == false {
                return index
            }
        }
        return 0
    }
    
    func completeCurrentWorkOut() {
        for index in 0..<primaryCountableObjects.count {
            if index == currentIndex {
                primaryCountableObjects[index].isDone = true
                return
            }
        }
    }
}

//MARK: - Configure UI
private extension BazelCounter {
    func drawPrimaryLayer() {
        primaryStrokeLayer.path = circularPath.cgPath
        primaryStrokeLayer.lineWidth = 20
        primaryStrokeLayer.fillColor = UIColor.clear.cgColor
        primaryStrokeLayer.lineCap = CAShapeLayerLineCap.round
        primaryStrokeLayer.strokeEnd = 0
        primaryStrokeLayer.position = view.center
        primaryStrokeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi/2, 0, 0, 1)
        view.layer.addSublayer(primaryStrokeLayer)
    }
    
    func drawSecondaryLayer() {
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 120, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        secondaryStrokeLayer.path = circularPath.cgPath
        secondaryStrokeLayer.lineWidth = 10
        secondaryStrokeLayer.fillColor = UIColor.clear.cgColor
        secondaryStrokeLayer.lineCap = CAShapeLayerLineCap.round
        secondaryStrokeLayer.strokeEnd = 0
        secondaryStrokeLayer.position = view.center
        secondaryStrokeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi/2, 0, 0, 1)
        view.layer.addSublayer(secondaryStrokeLayer)
    }
    
    func drawTrackLayer() {
        trackLayer.path = circularPath.cgPath
        trackLayer.lineWidth = 20
        trackLayer.fillColor = UIColor.black.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        trackLayer.position = view.center
        view.layer.addSublayer(trackLayer)
    }
    
    func drawPulsingLayer() {
        let circularPathforPulsingLayer = UIBezierPath(arcCenter: .zero, radius: 125, startAngle: 0, endAngle:  2 * CGFloat.pi, clockwise: true)
        pulsingLayer.path = circularPathforPulsingLayer.cgPath
        pulsingLayer.strokeColor = UIColor.clear.cgColor
        pulsingLayer.lineCap = CAShapeLayerLineCap.round
        pulsingLayer.position = view.center
        view.layer.addSublayer(pulsingLayer)
    }
}
