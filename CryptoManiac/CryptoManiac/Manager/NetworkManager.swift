//
//  NetworkManager.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

enum RESTApiRequestOption: String {
    case symbol
    case candle
}

enum RESTApiRequestErrorMessage: String, Error {
    case wrongUrl = "Cannot access to the URL. Please try another URL"
    case cannotFetchData = "Cannot fetch required data"
}


class NetworkManager {
    
    static let shared = NetworkManager()
    
    func requestSymbols(for requestOption: RESTApiRequestOption, completionHandler: @escaping (Result<[CryptoSymbolResponse]?, RESTApiRequestErrorMessage>) -> Void) {
        var urlString = Constants.finnHubBaseUrl
        
        switch requestOption {
        case .symbol:
            urlString += "\(RESTApiRequestOption.symbol.rawValue)?"
            urlString += "exchange=binance&"
            break
            
        case .candle:
            urlString += "\(RESTApiRequestOption.candle.rawValue)?"
            break
        }
        
        urlString += "\(Constants.finnHubApiKey)"
        guard let url = URL(string: urlString) else {return completionHandler(.failure(.wrongUrl))}
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else { return completionHandler(.failure(.cannotFetchData)) }
            
            do {
                let fetchedData = try JSONDecoder().decode([CryptoSymbolResponse].self, from: data)
                completionHandler(.success(fetchedData))
            } catch {
                completionHandler(.failure(.cannotFetchData))
            }
        }.resume()
    }
    
    func requestCandles() {
        
    }
    
    func requestNewsAboutCryptoCurrency(completionHandler: @escaping (Result<NewsResponse?, RESTApiRequestErrorMessage>) -> Void) {
        
        var urlString = Constants.newsBaseUrl
        urlString += "q=blockChain&"
        urlString += Constants.newsApiKey
        
        print(urlString)
        
        guard let url = URL(string: urlString) else {return completionHandler(.failure(.wrongUrl))}
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else { return completionHandler(.failure(.cannotFetchData)) }
            
            do {
                let fetchedData = try JSONDecoder().decode(NewsResponse.self, from: data)
                completionHandler(.success(fetchedData))
            } catch {
                print(error)
                completionHandler(.failure(.cannotFetchData))
            }
        }.resume()
    }
    
    let cache = NSCache<NSString, UIImage>()
    
    func loadImage(urlString: String, completionHandler: @escaping (UIImage?) -> Void) {
        
        let cacheKey = NSString(string: urlString)
        
        if let image = self.cache.object(forKey: cacheKey) {
            completionHandler(image)
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) {[weak self] data, response, error in
            guard let self = self,
                  error == nil,
                  let response = response as? HTTPURLResponse, response.statusCode == 200,
                  let data = data,
                  let image = UIImage(data: data) else {
                
                    completionHandler(nil)
                    return
                }
            
            self.cache.setObject(image, forKey: cacheKey)
            completionHandler(image)
            
        }.resume()
    }
    
    func requestFromBithumb(completionHandler: @escaping (Result<BithumbTickerResponse, Error>) -> Void) {
        let urlString = Constants.url
        guard let url = URL(string: urlString) else {return }
        print(url)
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data else {return }
            do {
                let bithumTickerResponse = try JSONDecoder().decode(BithumbTickerResponse.self, from: data)
                completionHandler(.success(bithumTickerResponse))
            } catch {
                completionHandler(.failure(error))
            }
        }.resume()
        
    }
    
    private struct Constants {
        static let finnHubApiKey = "token=c3ecka2ad3ief4elg710"
        static let sandboxApiKey = "token=sandbox_c3ecka2ad3ief4elg71g"
        static let finnHubBaseUrl = "https://finnhub.io/api/v1/crypto/"
        
        static let newsApiKey = "apiKey=78bd744a9e784bd5b86c6904a41a5464"
        static let newsBaseUrl = "https://newsapi.org/v2/everything?"
        
        static let url = "https://api.bithumb.com/public/ticker/"
    }
    
   
}
