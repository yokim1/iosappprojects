//
//  CommentFloatingVC.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

enum FloatingVCType: String {
    case news = "News"
    case comments = "Comments"
}

class CommentFloatingVC: UIViewController {
    
    var tableView = UITableView()
    var comments: [Comment] = [
        Comment(coinTitle: "COS/USDT", title: "COS Matter",
                date: "2021-07-07", content: "ALL this matter to be free from centralized power"),
        Comment(coinTitle: "MCO/USDT", title: "MCO Matter",
                date: "2021-07-07", content: "ALL this matter to be free from centralized power"),
        
    ]
    var fechedComments: [Comment] = []
    var floatingVCType: FloatingVCType?
    var coin: String?
    
    init(about coinTitle: String = "") {
        super.init(nibName: nil, bundle: nil)
        self.coin = coinTitle
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        
        fetchComments()
        
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame         = view.bounds
        tableView.delegate      = self
        tableView.dataSource    = self
        
        tableView.rowHeight = CommentsCell.preferredHeight
        tableView.register(CommentsCell.self, forCellReuseIdentifier: CommentsCell.identifier)
        
        tableView.register(FloatingPanelHeaderView.self, forHeaderFooterViewReuseIdentifier: FloatingPanelHeaderView.identifier)
        tableView.removeExcessCells()
    }
    
    private func fetchComments() {
        fechedComments = comments.filter { comment in
            comment.coinTitle == coin
        }
    }
    
}

extension CommentFloatingVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fechedComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommentsCell.identifier, for: indexPath) as? CommentsCell else { return UITableViewCell() }
        cell.configure(with: fechedComments[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: FloatingPanelHeaderView.identifier) as? FloatingPanelHeaderView else {return UIView()}
        //guard let titleType = floatingVCType else {return UIView()}
        view.set(title: .comments, isButtonHidden: false)
        view.backgroundColor = .blue
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        FloatingPanelHeaderView.preferredHeight
    }
}

extension CommentFloatingVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        print("Comment is tapped")
    }
}
