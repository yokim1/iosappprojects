//
//  ViewController.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import SafariServices
import UIKit

struct Comment: Codable {
    let coinTitle: String
    let title: String
    let date: String
    let content: String
}

class NewsFloatingVC: UIViewController {
    
    var tableView = UITableView()
    var newsArticles: [NewsArticle] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        fetchNewsData()
        
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame         = view.bounds
        tableView.delegate      = self
        tableView.dataSource    = self
        
        tableView.rowHeight     = NewsListCell.preferredHeight
        tableView.register(NewsListCell.self, forCellReuseIdentifier: NewsListCell.identifier)
        
        
        tableView.register(FloatingPanelHeaderView.self, forHeaderFooterViewReuseIdentifier: FloatingPanelHeaderView.identifier)
        tableView.removeExcessCells()
    }
    
    private func fetchNewsData() {
        NetworkManager.shared.requestNewsAboutCryptoCurrency { result in
            switch result {
            case.success(let response):
                DispatchQueue.main.async {
                    self.newsArticles = response?.articles ?? []
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension NewsFloatingVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return newsArticles.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NewsListCell.identifier, for: indexPath) as? NewsListCell else {return UITableViewCell() }
        cell.configure(with: newsArticles[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: FloatingPanelHeaderView.identifier) as? FloatingPanelHeaderView else {return UIView()}
        view.set(title: .news)
        view.backgroundColor = .blue
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        FloatingPanelHeaderView.preferredHeight
    }
}

extension NewsFloatingVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let url = URL(string: newsArticles[indexPath.row].url) else { return }
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true, completion: nil)
    }
}
