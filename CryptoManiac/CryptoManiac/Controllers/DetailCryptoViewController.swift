//
//  DetailCryptoViewController.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//
import FloatingPanel
import UIKit

protocol DetailCryptoViewControllerDelegate: AnyObject{
    func dismissButtonDidTapped(vc: DetailCryptoViewController)
}

class DetailCryptoViewController: UIViewController, FloatingPanelControllerDelegate {

    var coinTitle: String?
    var isViewControllerPushed: Bool?
    var delegate: DetailCryptoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        view.backgroundColor = .systemBackground
        title = coinTitle
        configureNavigationBarCloseButton()
        configureFloatingPanel()
        
    }
    
    private func configureNavigationBarCloseButton() {
        guard let isDetailFavoriteVCPushed = isViewControllerPushed else { return }
        if isDetailFavoriteVCPushed {
            let removeButton = UIBarButtonItem(title: "Remove", style: .plain, target: self, action: #selector(removeFromFavoriteListButtonDidTapped))
            
            navigationItem.rightBarButtonItems = [removeButton]
        } else {
            let addButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addToFavoriteListButtonDidTapped))
            let closeButton = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(didTapClose))
            
            navigationItem.leftBarButtonItems = [addButton]
            navigationItem.rightBarButtonItems = [closeButton]
        }
    }
    
    @objc private func removeFromFavoriteListButtonDidTapped() {
        print("remove")
    }
    
    @objc private func addToFavoriteListButtonDidTapped() {
        print("FavoriteList add button Tapped")
    }
    
    @objc func didTapClose() {
        delegate?.dismissButtonDidTapped(vc: self)
    }
    
    private func configureFloatingPanel() {
        let vc = CommentFloatingVC(about: coinTitle ?? "")
        let panel = FloatingPanelController()
        panel.delegate = self
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
    }
}
