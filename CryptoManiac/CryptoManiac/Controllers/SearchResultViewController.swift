//
//  SearchResultViewController.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

protocol SearchResultViewControllerDelegate: AnyObject {
    func SearchResultViewControllerDidSelect(vc: DetailCryptoViewController)
}

class SearchResultViewController: UIViewController {
    
    let tableView               = UITableView()
    static var coinSymbols             = [CryptoSymbolResponse]()
    var filteredCoinSymbols     = [CryptoSymbolResponse]()
    var delegate: SearchResultViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchCoinSymbols()
        configureTableView()
    }
    
    private func fetchCoinSymbols() {
//        NetworkManager.shared.requestSymbols(for: .symbol) { result in
//            switch result {
//            case .success(let fetchedData):
//                DispatchQueue.main.async {
//                    self.coinSymbols = fetchedData?.filter({ response in
//                        response.displaySymbol.contains("/USDT")
//                    }) ?? []
//                    self.tableView.reloadData()
//                }
//                break
//
//            case .failure(let error):
//                print(error)
//                break
//            }
//        }
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame         = view.bounds
        tableView.delegate      = self
        tableView.dataSource    = self
        tableView.rowHeight     = SearchResultCell.prefferedHeight
        tableView.register(SearchResultCell.self, forCellReuseIdentifier: SearchResultCell.identifier)
        tableView.removeExcessCells()
    }
    
    func update(with searchKeyword: String) {
        filteredCoinSymbols = SearchResultViewController.coinSymbols.filter { coinSymbol in
            coinSymbol.displaySymbol.lowercased().contains(searchKeyword.lowercased())
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        print(filteredCoinSymbols.first?.displaySymbol)
    }
}

extension SearchResultViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filteredCoinSymbols.isEmpty ? SearchResultViewController.coinSymbols.count : filteredCoinSymbols.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.identifier, for: indexPath) as? SearchResultCell else {return UITableViewCell()}
        
        if filteredCoinSymbols.isEmpty {
            cell.configure(with: SearchResultViewController.coinSymbols[indexPath.row])
        } else {
            cell.configure(with: filteredCoinSymbols[indexPath.row])
        }
        
        return cell
    }
}

extension SearchResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = DetailCryptoViewController()
        
        vc.delegate = self
        vc.isViewControllerPushed = false
        
        if filteredCoinSymbols.isEmpty {
            vc.coinTitle = SearchResultViewController.coinSymbols[indexPath.row].displaySymbol
            delegate?.SearchResultViewControllerDidSelect(vc: vc)
        } else {
            vc.coinTitle = filteredCoinSymbols[indexPath.row].displaySymbol
            delegate?.SearchResultViewControllerDidSelect(vc: vc)
        }
    }
}

extension SearchResultViewController: DetailCryptoViewControllerDelegate {
    func dismissButtonDidTapped(vc: DetailCryptoViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
}
