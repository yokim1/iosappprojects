//
//  ViewController.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/05.
//

import FloatingPanel
import UIKit

class MyFavoriteCryptoListViewController: UIViewController, SearchResultViewControllerDelegate {
    func SearchResultViewControllerDidSelect(vc: DetailCryptoViewController) {
        let navVC = UINavigationController(rootViewController: vc)
        present(navVC, animated: true, completion: nil)
    }
    
    
    let tableView       = UITableView()
    var favoriteCoins: [CryptoSymbolResponse] = [
        CryptoSymbolResponse(description: "PersonPeopleProgram", displaySymbol: "PPP/USDT", symbol: "JOJOJo")
    ]
    var coinSymbols:[CryptoSymbolResponse] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title = "Crypto Currency"
        navigationController?.navigationBar.prefersLargeTitles = true
        setUpTitleView()
        fetchCoinSymbols()
        configureTableView()
        configureSearchResultVC()
        configureFloatingPanel()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
    private func setUpTitleView() {
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: navigationController?.navigationBar.frame.size.height ?? 100))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width - 20, height: titleView.frame.size.height))
        label.text = "CryptoCurrency"
        label.font = .systemFont(ofSize: 32, weight: .medium)
        titleView.addSubview(label)
        navigationItem.titleView = titleView
    }
    
    private func fetchCoinSymbols() {
        NetworkManager.shared.requestSymbols(for: .symbol) { result in
            switch result {
            case .success(let fetchedData):
                DispatchQueue.main.async {
                    SearchResultViewController.coinSymbols = fetchedData?.filter({ response in
                        response.displaySymbol.contains("/USDT")
                    }) ?? []
                    //self.tableView.reloadData()
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }

    private func configureTableView() {
        view.addSubview(tableView)
       
        tableView.frame         = view.bounds
        tableView.dataSource    = self
        tableView.delegate      = self
        tableView.rowHeight     = MyFavoriteCryptoListCell.preferredHeight
        tableView.register(MyFavoriteCryptoListCell.self, forCellReuseIdentifier: MyFavoriteCryptoListCell.identifier)
        tableView.removeExcessCells()
    }
    
    private func configureSearchResultVC() {
//        let resultVC = SearchResultViewController()
//        let searchVC = UISearchController(searchResultsController: resultVC)
//        resultVC.delegate = self
//        searchVC.searchResultsUpdater   = self
//        navigationItem.searchController = searchVC
        
        let resultVC = SearchResultViewController()
        resultVC.delegate = self
        let searchVC = UISearchController(searchResultsController: resultVC)
        searchVC.searchResultsUpdater = self
        navigationItem.searchController = searchVC
    }
    
    private func configureFloatingPanel() {
//        let vc = NewsFloatingVC()
//        let panel = FloatingPanelController()
//        panel.delegate = self
//        panel.set(contentViewController: vc)
//        panel.addPanel(toParent: self)
//        panel.track(scrollView: vc.tableView)
//
        let vc = NewsFloatingVC()
        let panel = FloatingPanelController()
        panel.delegate = self
        panel.surfaceView.backgroundColor = .red
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
    }
    
    private func setUpFloatingPanel() {
        let vc = NewsFloatingVC()
        let panel = FloatingPanelController()
        panel.delegate = self
        panel.surfaceView.backgroundColor = .red
        panel.set(contentViewController: vc)
        panel.addPanel(toParent: self)
        panel.track(scrollView: vc.tableView)
    }
}

extension MyFavoriteCryptoListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchKewWord = searchController.searchBar.text,
              let searchVC      = searchController.searchResultsController as? SearchResultViewController
              else { return }
        
        if searchKewWord.isEmpty { return  }
        
        searchVC.update(with: searchKewWord)
    }
}

extension MyFavoriteCryptoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteCoins.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyFavoriteCryptoListCell.identifier, for: indexPath) as? MyFavoriteCryptoListCell else { return UITableViewCell() }
    
        cell.configure(with: favoriteCoins[indexPath.row])

        return cell
    }
}

extension MyFavoriteCryptoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailCryptoVC = DetailCryptoViewController()
        detailCryptoVC.coinTitle = favoriteCoins[indexPath.row].displaySymbol
        detailCryptoVC.isViewControllerPushed = true
        navigationController?.pushViewController(detailCryptoVC, animated: true)
    }
}


extension MyFavoriteCryptoListViewController: FloatingPanelControllerDelegate {
    func floatingPanelDidChangeState(_ fpc: FloatingPanelController) {
        navigationItem.titleView?.isHidden = fpc.state == .full
        print(fpc.state)
    }
}


