//
//  File.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

class NewsListCell: UITableViewCell {
    static let identifier = "CommentListCell"
    static let preferredHeight: CGFloat = 120
    
    let title = TitleLabel(textAlignment: .left, fontSize: 16)
    let date = SecondaryTitle(textAlignment: .left, fontSize: 12)
    let newsImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with news: NewsArticle) {
        self.title.text = news.title
        self.date.text = news.publishedAt
        
        NetworkManager.shared.loadImage(urlString: news.urlToImage ?? "") { image in
            DispatchQueue.main.async {
                self.newsImageView.image = image
            }
        }
        
        newsImageView.backgroundColor = .gray
    }
    
    private func configureUI() {
        addSubview(title)
        addSubview(date)
        addSubview(newsImageView)
        
        title.numberOfLines = 4
        newsImageView.contentMode = .scaleToFill
        newsImageView.translatesAutoresizingMaskIntoConstraints = false
        
        let padding: CGFloat = 10
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            title.trailingAnchor.constraint(equalTo: newsImageView.leadingAnchor, constant: -padding),
            
            date.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            date.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            
            newsImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            newsImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            newsImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            newsImageView.widthAnchor.constraint(equalToConstant: NewsListCell.preferredHeight),
            newsImageView.heightAnchor.constraint(equalToConstant: NewsListCell.preferredHeight)
        ])
    }
}
