//
//  File.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

class CommentsCell: UITableViewCell {
    static let identifier = "CommentsCell"
    static let preferredHeight: CGFloat = 120
    
    let title = TitleLabel(textAlignment: .left, fontSize: 16)
    let date = SecondaryTitle(textAlignment: .left, fontSize: 12)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with comments: Comment) {
        self.title.text = comments.title
        self.date.text = comments.date
        
    }
    
    private func configureUI(){
        addSubview(title)
        addSubview(date)
        
        title.numberOfLines = 2
        
        let padding: CGFloat = 10
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            
            date.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            date.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
        ])
    }
}
