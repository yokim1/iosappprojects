//
//  SearchResultTableViewCell.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

class SearchResultCell: UITableViewCell {
    static let identifier               = "SearchResultCell"
    static let prefferedHeight:CGFloat  = 70
    
    private var coinSymbol          = TitleLabel(textAlignment: .left, fontSize: 17)
    private var coinDescription     = SecondaryTitle(textAlignment: .left, fontSize: 13)

    private var coinPriceChart      = StockChartView()
    
    private var coinPrice           = SecondaryTitle(textAlignment: .center, fontSize: 15)
    private var cointPriceChangePercentage = PercentageChangeLabel(textAlignment: .right, fontSize: 14)
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with coinSymbol: CryptoSymbolResponse) {
        self.coinSymbol.text = coinSymbol.displaySymbol
        self.coinDescription.text = coinSymbol.description
        
        self.coinPrice.text = "\(12345)"
        self.cointPriceChangePercentage.text = "\(67890) %"
        self.cointPriceChangePercentage.backgroundColor = .systemGreen
    }
    
    private func configureUI() {
        addSubview(coinSymbol)
        addSubview(coinDescription)
        addSubview(coinPriceChart)
        addSubview(coinPrice)
        addSubview(cointPriceChangePercentage)
        
        let paddingBetweenTextAndCell: CGFloat      = 8
        let paddingBetweenTextAndChart: CGFloat     = 30
            
        NSLayoutConstraint.activate([
            coinSymbol.topAnchor.constraint(equalTo: topAnchor, constant: paddingBetweenTextAndCell),
            coinSymbol.leadingAnchor.constraint(equalTo: leadingAnchor, constant: paddingBetweenTextAndCell),
            //coinSymbol.widthAnchor.constraint(equalToConstant: )
            
            coinDescription.topAnchor.constraint(equalTo: coinSymbol.bottomAnchor, constant: paddingBetweenTextAndCell),
            coinDescription.leadingAnchor.constraint(equalTo: leadingAnchor, constant: paddingBetweenTextAndCell),
            
            coinPriceChart.centerYAnchor.constraint(equalTo: centerYAnchor),
            coinPriceChart.trailingAnchor.constraint(equalTo: coinPrice.leadingAnchor, constant: -paddingBetweenTextAndChart),
            coinPriceChart.widthAnchor.constraint(equalToConstant: SearchResultCell.prefferedHeight + paddingBetweenTextAndCell),
            coinPriceChart.heightAnchor.constraint(equalToConstant: SearchResultCell.prefferedHeight - paddingBetweenTextAndCell),
            
            coinPrice.topAnchor.constraint(equalTo: topAnchor, constant: paddingBetweenTextAndCell),
            //coinPrice.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -paddingBetweenTextAndCell),
            coinPrice.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -paddingBetweenTextAndCell),
            
            cointPriceChangePercentage.topAnchor.constraint(equalTo: coinPrice.bottomAnchor, constant: paddingBetweenTextAndCell),
            //cointPriceChangePercentage.topAnchor.constraint(equalTo: centerYAnchor, constant: paddingBetweenTextAndCell),
            cointPriceChangePercentage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -paddingBetweenTextAndCell),
        ])
    }
}
