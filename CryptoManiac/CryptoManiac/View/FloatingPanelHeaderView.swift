//
//  File.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

class FloatingPanelHeaderView: UITableViewHeaderFooterView {
    static let identifier = "FloatingPaenlHeaderCell"
    static let preferredHeight: CGFloat = 50
    
    private let title = TitleLabel(textAlignment: .left, fontSize: 30)
//    private let button = UIButton(type: .contactAdd, primaryAction: UIAction(title: "Add", image: UIImage(systemName: "person"), identifier: nil, discoverabilityTitle: nil, attributes: .disabled, state: .on, handler: { _ in
//        print("tapped")
//    }))
    private let button = UIButton()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(title: FloatingVCType, isButtonHidden: Bool = true) {
        self.title.text = title.rawValue
        self.button.isHidden = isButtonHidden
    }
    
    private func configureUI() {
        addSubview(title)
        addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.setImage(UIImage(systemName: "bubble.left.and.bubble.right"), for: .normal)
        button.addTarget(self, action: #selector(buttonDidTapped), for: .touchUpInside)
        
        let padding: CGFloat = 10
        
        NSLayoutConstraint.activate([
            title.centerYAnchor.constraint(equalTo: centerYAnchor),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            
            button.centerYAnchor.constraint(equalTo: centerYAnchor),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            button.heightAnchor.constraint(equalToConstant: 30),
            button.widthAnchor.constraint(equalToConstant: 30),
        ])
    }
    
    @objc private func buttonDidTapped() {
        print("Tapped Button")
    }
}
