//
//  SecondaryTitle.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

class SecondaryTitle: UILabel {

    override init(frame: CGRect) {
        super.init(frame: .zero)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(textAlignment: NSTextAlignment, fontSize: CGFloat) {
        self.init(frame: .zero)
        self.textAlignment = textAlignment
        self.font = UIFont.systemFont(ofSize: fontSize, weight: .medium)
    }
    
    private func configureUI() {
        textColor = .secondaryLabel
        translatesAutoresizingMaskIntoConstraints = false
    }
}
