//
//  CryptoSymbolResponse.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import Foundation

struct CryptoSymbolResponse: Decodable {
    let description: String
    let displaySymbol: String
    let symbol: String
}

struct CryptoCandle: Decodable {
    let c: [Double]
    let h: [Double]
    let l: [Double]
    let o: [Double]
    let s: String
    let t: [Double]
    let v: [Double]
}




struct BithumbTickerResponse: Decodable {
    let status: String
    let data: BithumbTickerData
}

//struct BTC: Decodable {
//    let BTC: BithumbTickerData
//}

struct BithumbTickerData: Decodable {
    let opening_price : String
    let closing_price : String
    let min_price : String
    let max_price : String
    let units_traded : String
    let acc_trade_value : String
    let prev_closing_price : String
    let units_traded_24H : String
    let acc_trade_value_24H : String?
    let fluctate_24H : String
    let fluctate_rate_24H : String
    let date : String
}

//
//"opening_price":"39434000",
//"closing_price":"39267000",
//"min_price":"39160000",
//"max_price":"39444000",
//"units_traded":"252.12802154",
//"acc_trade_value":"9905201074.0802",
//"prev_closing_price":"39416000",
//"units_traded_24H":"2841.98694079",
//"acc_trade_value_24H":"113746570048.5479",
//"fluctate_24H":"-1836000",
//"fluctate_rate_24H":"-4.47"
//
//"data": {
//    "BTC": {
//        "opening_price":"39434000",
//        "closing_price":"39267000",
//        "min_price":"39160000",
//        "max_price":"39444000",
//        "units_traded":"252.12802154",
//        "acc_trade_value":"9905201074.0802",
//        "prev_closing_price":"39416000",
//        "units_traded_24H":"2841.98694079",
//        "acc_trade_value_24H":"113746570048.5479",
//        "fluctate_24H":"-1836000",
//        "fluctate_rate_24H":"-4.47"
//}
