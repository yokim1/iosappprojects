//
//  NewsResponse.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import Foundation

struct NewsResponse: Decodable {
    let status: String
    let totalResults: Int
    let articles: [NewsArticle]
}

struct NewsArticle: Decodable {
    let author: String?
    let title: String
    let description: String
    let url: String
    let urlToImage: String?
    let publishedAt: String //"2021-06-09T12:04:40Z",
    let content: String
}
