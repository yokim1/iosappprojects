//
//  UItableView+Extension.swift
//  CryptoManiac
//
//  Created by 김윤석 on 2021/07/06.
//

import UIKit

extension UITableView {
    func removeExcessCells() {
        tableFooterView = UIView(frame: .zero)
    }
}
